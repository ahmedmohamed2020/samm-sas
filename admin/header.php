<header class="header">
<div dir="rtl" style="text-align:center;background-color:#086135;">
                <span class="mr">
                    <span><i class="fa fa-calendar" aria-hidden="true" style="font-size: 15px; font-weight: bold;"></i></span>
                    <span id="dte"></span>
                </span>
                <span class="mr">
                    <span><i class="fa fa-calendar" aria-hidden="true" style="font-size:15px; font-weight:bold;"></i></span>
                    <span id="dt"></span>
                </span>
                <span class="mr">
                    <span><i class="fa fa-clock-o" aria-hidden="true" style="font-size:15px;font-weight:bold;"></i></span>

                    <span id="tm"></span>
                </span>
            </div>
        <div class="logo-container header-right">
          <a href="../" class="logo">
            <img src="../assets/images/logo.png" height="35" alt="Porto Admin" />
          </a>
          
        </div>
      
        <!-- start: search & user box -->
        <div class="header-left" style="direction: ltr;">
      
          <ul class="notifications">
            <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
          </div>
                  
          </ul>
      
          <span class="separator"></span>
      
          <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
              <figure class="profile-picture">
                <img src="../assets/images/<?php echo $_SESSION['pic']; ?>" alt="Joseph Doe" class="img-circle" />
              </figure>
              <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                <span class="name"><?php echo $_SESSION['admin']; ?></span>
                <span class="role"><?php echo $_SESSION['job']; ?></span>
              </div>
      
              <i class="fa custom-caret"></i>
            </a>
      
            <div class="dropdown-menu">
              <ul class="list-unstyled">
                <li class="divider"></li>
                <li>
                  <a role="menuitem" tabindex="-1" href="edit_user2.php?id=<?php echo $_SESSION['id']; ?>"><i class="fa fa-user"></i>البروفايل</a>
                </li>
                <li>
                  <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> شاشة القفل</a>
                </li>
                <li>
                  <a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> تسجيل خروج</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- end: search & user box -->
      </header>