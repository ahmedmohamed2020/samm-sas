<?php
session_start();
include("../conn.php");


if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$sql="SELECT * from info ";
$result=mysqli_query($con,$sql);
$inc = 1;
?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

  </head>
  <body>
    <section class="body" style="direction: rtl;">

      <!-- start: header -->
      <header class="header">
      <div dir="rtl" style="text-align:center;background-color:#086135;">
                <span class="mr">
                    <span><i class="fa fa-calendar" aria-hidden="true" style="font-size: 15px; font-weight: bold;"></i></span>
                    <span id="dte"></span>
                </span>
                <span class="mr">
                    <span><i class="fa fa-calendar" aria-hidden="true" style="font-size:15px; font-weight:bold;"></i></span>
                    <span id="dt"></span>
                </span>
                <span class="mr">
                    <span><i class="fa fa-clock-o" aria-hidden="true" style="font-size:15px;font-weight:bold;"></i></span>

                    <span id="tm"></span>
                </span>
            </div>
        <div class="logo-container header-right">
          <a href="../" class="logo">
            <img src="../assets/images/logo.png" height="35" alt="Porto Admin" />
          </a>
          
        </div>
      
        <!-- start: search & user box -->
        <div class="header-left" style="direction: ltr;">
      
          
      
      
      
          <ul class="notifications">
            <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
          </div>
                  
          </ul>
      
          <span class="separator"></span>
      
          <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
              <figure class="profile-picture">
                <img src="../assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="../assets/images/!logged-user.jpg" />
              </figure>
              <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
                <span class="name">Admin</span>
                <span class="role">administrator</span>
              </div>
      
              <i class="fa custom-caret"></i>
            </a>
      
            <div class="dropdown-menu">
              <ul class="list-unstyled">
                <li class="divider"></li>
                <li>
                  <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>
                </li>
                <li>
                  <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>
                </li>
                <li>
                  <a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- end: search & user box -->
      </header>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
         
          <!-- start: page -->
             <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
                <div>
                   <a class='btn btn-primary btn-xs ' href='add_company1.php' style="float: left;"><i class="fa    fa-plus" aria-hidden="true"></i>   اضافة شركة
                 </a>
                </div>
                <h2 class="panel-title">الشركات</h2>
              </header>
             <div class="panel-body table-responsive">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                <thead>
                    <tr>
                      <th>م</th>
                      <th>اسم الشركة</th>
                      <th>العنوان</th>
                      <th style="display: none;">الهاتف</th>
                      <th style="display: none;">الايميل</th>
                      <th style="display: none;">نوع النشاط</th>
                      <th style="display: none;">عدد الفروع</th>
                      <th style="display: none;">مدة الاشتراك</th>
                      <th >الحالة</th>
                      <th style="min-width: 330px !important">.......</th>
                    </tr>
                  </thead>
                  <tbody>
                       <?php    while ($row=mysqli_fetch_array($result)) {     ?>
                    <tr class="gradeX">
                       <td><?php echo $inc; ?></td>
                      <td><?php echo $row['name']; ?></td>
                      <td><?php echo $row['address']; ?> </td>
                      <td style="display: none;"><a class="btn btn-warning btn-xs" onclick="return gtag_report_conversion('tel:<?php echo $row['tel']; ?>');" href="tel:<?php echo $row['tel']; ?>"><?php echo $row['tel']; ?></a></td>
                      <td style="display: none;"><?php echo $row['email']; ?></td>
                      <td style="display: none;"><?php echo $row['activity']; ?></td>
                      <td style="display: none;"><?php echo $row['branchs']; ?></td>
                      <td style="display: none;"><?php echo $row['subscrip']; ?> </td>
                      <td ><?php echo $row['company_stat']; ?></td>
                      <td style="width: 330px">
                    <?php $inc ++; ?>

                 <?php    if($row['company_stat'] == 'تسجيل جديد' || $row['company_stat'] == 'ايقاف') { ?>           
                 <a class='btn btn-danger btn-xs ' href='acsept_company.php?id=<?php echo $row['id']; ?>'><i class="fa    fa-plus" aria-hidden="true"></i>   تفعيل
                 </a>
                 <?php    }  ?>
                 <?php    if($row['company_stat'] == 'تمت الموافقة') { ?>           
                 <a class='btn btn-success btn-xs ' href=''><i class="fa    fa-plus" aria-hidden="true"></i>   مفعلة
                 </a>
                 <?php    }  ?>
                 <a href="hold_company.php?id=<?php echo $row['id']; ?>" class="btn btn-info btn-xs " supp_id="2"><i class="fa    fa-pencil" aria-hidden="true"></i> إيقاف</a>
                 <a href="edit_company1.php?id=<?php echo $row['id']; ?>" class="btn btn-primary btn-xs " supp_id="2"><i class="fa    fa-pencil" aria-hidden="true"></i>   تعديل</a>
                 <a href="delete_company.php?id=<?php echo $row['id']; ?>" class="btn btn-warning btn-xs delete" supp_id="2"><i class="fa   fa-times" aria-hidden="true"></i> حزف</a>
                 <a href="emps_company.php?id=<?php echo $row['id']; ?>" class="btn btn-default btn-xs " supp_id="2"><i class="fa   fa-users" aria-hidden="true"></i> الموظفين</a>
                           
                      </td>
                    </tr>

                       <?php    }  ?>
                       
                  </tbody>
              </table>
            </div>
          </div>
          <!-- page end-->
        </div>
            </section>

            
           
      </div>

      
    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>


    <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../assets/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;font-size: revert;    font-weight: bold;">';
      sOut += '<tr><td>اسم الشركة :</td><td>' + aData[1] + ' </td></tr>';
      sOut += '<tr><td>العنوان :</td><td>' + aData[2] + ' </td></tr>';
      sOut += '<tr><td>الهاتف :</td><td>' + aData[3] + ' </td></tr>';
      sOut += '<tr><td>الايميل  :</td><td>' + aData[4] + ' </td></tr>';
      sOut += '<tr><td>نوع النشاط  :</td><td>' + aData[5] + ' </td></tr>';
      sOut += '<tr><td>عدد الفروع  :</td><td>' + aData[6] + ' </td></tr>';
      sOut += '<tr><td>مدة الاشتلراك  :</td><td>' + aData[7] + ' </td></tr>';
      sOut += '<tr><td>الحالة  :</td><td>' + aData[8] + ' </td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="../assets/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "../assets/advanced-datatable/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "../assets/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });
  </script>
  </body>
</html>