 <aside id="sidebar-left" class="sidebar-left">
        
          <div class="sidebar-header">
            <div class="sidebar-title">
              Navigation
            </div>
            <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
              <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
          </div>
        
          <div class="nano">
            <div class="nano-content">
              <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                  <li>
                    <a href="index.php">
                      <i class="fa fa-home" aria-hidden="true"></i>
                      <span>لوحة التحكم</span>
                    </a>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-copy" aria-hidden="true"></i>
                      <span>الشركات</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="index.php">
                           قائمة الشركات
                        </a>
                      </li>
                      
                    </ul>
                  </li>
              
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-tasks" aria-hidden="true"></i>
                      <span>الطلبات</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="new_emp.php">
                           طلبات اضافة موظفين
                        </a>
                      </li>
                   
                    </ul>
                  </li>
                      
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-list-alt" aria-hidden="true"></i>
                      <span>المدن</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="city.php">
                           قائمة المدن
                        </a>
                      </li>
                    </ul>
                  </li>
                  <!-- start: page 
                  <li class="nav-parent  nav-active">
                    <a>
                      <i class="fa fa-table" aria-hidden="true"></i>
                      <span>Tables</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="tables-basic.html">
                           Basic
                        </a>
                      </li>
                      <li class="nav-active">
                        <a href="tables-advanced.html">
                           Advanced
                        </a>
                      </li>
                      <li>
                        <a href="tables-responsive.html">
                           Responsive
                        </a>
                      </li>
                      <li>
                        <a href="tables-editable.html">
                           Editable
                        </a>
                      </li>
                      <li>
                        <a href="tables-ajax.html">
                           Ajax
                        </a>
                      </li>
                      <li>
                        <a href="tables-pricing.html">
                           Pricing
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                      <span>Maps</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="maps-google-maps.html">
                           Basic
                        </a>
                      </li>
                      <li>
                        <a href="maps-google-maps-builder.html">
                           Map Builder
                        </a>
                      </li>
                      <li>
                        <a href="maps-vector.html">
                           Vector
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-columns" aria-hidden="true"></i>
                      <span>Layouts</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="layouts-default.html">
                           Default
                        </a>
                      </li>
                      <li>
                        <a href="layouts-boxed.html">
                           Boxed
                        </a>
                      </li>
                      <li>
                        <a href="layouts-menu-collapsed.html">
                           Menu Collapsed
                        </a>
                      </li>
                      <li>
                        <a href="layouts-scroll.html">
                           Scroll
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-align-left" aria-hidden="true"></i>
                      <span>Menu Levels</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a>First Level</a>
                      </li>
                      <li class="nav-parent">
                        <a>Second Level</a>
                        <ul class="nav nav-children">
                          <li class="nav-parent">
                            <a>Third Level</a>
                            <ul class="nav nav-children">
                              <li>
                                <a>Third Level Link #1</a>
                              </li>
                              <li>
                                <a>Third Level Link #2</a>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <a>Second Level Link #1</a>
                          </li>
                          <li>
                            <a>Second Level Link #2</a>
                          </li>
                        </ul>
                      </li>
                      -->
                    </ul>
                  </li>
                  
                </ul>
              </nav>
        
              
        
              <div class="sidebar-widget widget-stats">
                <div class="widget-header">
                 
                  <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                  <ul>
                    
                  </ul>
                </div>
              </div>
            </div>
        
          </div>
        
        </aside>
<!-- Vendor -->
<script src="../assets/vendor/jquery/jquery.js"></script>
<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="lib/jquery/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
            setInterval(loadDateTime, 1000);
            function loadDateTime() {
                const event = new Date();
                const options = {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                }
                $('#dte').html(event.toLocaleDateString('ar-EG', options));
                $('#dt').html(event.toLocaleDateString('ar-SA', options));
                $('#tm').html(event.toLocaleTimeString('ar-SA'));
            }
        })
</script>