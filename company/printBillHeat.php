
<?php

include("../conn.php");
include("../barcode.php");
include("../num_to_ar.php");
$generator = new barcode_generator();
$options = array();
session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];

$Bill_id=$_GET['id'];
$pic=$_SESSION['Cpic'];
// $sql1="SELECT *FROM  info ";
// $result1 = mysqli_query($con,$sql1);
// $row1=mysqli_fetch_array($result1);

// $sql2="SELECT *FROM  carts,emp,customer where customer.customer_id=carts.carts_cus and emp_id=carts_emp_id and carts_id= ".$_GET['id'];
// $result2 = mysqli_query($con,$sql2);
// $row2=mysqli_fetch_array($result2);


$sql4="SELECT * FROM  bills where id=".$Bill_id;
$result4 = mysqli_query($con,$sql4);
$row2=mysqli_fetch_array($result4);
$urlBill = $serverName . 'printBillHeat.php?token=\'' . $row2['token'].'\'';

$sql3="SELECT * from bill_items where bill_id= ".$Bill_id;
$result3=mysqli_query($con,$sql3);
$sql4="SELECT * FROM  info where id=".$_SESSION['company_id'];
$result4 = mysqli_query($con,$sql4);
$result44=mysqli_fetch_row($result4);

$i=1;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>فاتورة مبيعات</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css2/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css2/adminlte.min.css">
  <link rel="stylesheet" href="../css2/print.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    span,b,div,th,td{
      font-size: 40px;
    }
    .rowss{
      direction: rtl;
      text-align: right;
      margin: 16px;
    }
    .algn{
      text-align: right;
    }
    .algnC{
      text-align: center;
    }
    .centerdv{
      display: flex;
    justify-content: center;
    }
    .titl{
      text-decoration: underline;
    font-size: 22px;
    }
    .backG{
      background-color: #d0d0d0;
    }
    .mr{
      margin-top: 32px;
    }
    .imgS{
      width:260px;
      height: 160px;
    }
    .imgSQR{
      width:235px;
      height: 235px;
    }
    svg{
      width: 350px !important;
      height: 300px !important;
      margin: -31px !important;
    }
  </style>
</head>
<body>
  <div class="page2">

<div class="">
  <!-- Main content -->
  <section >
    <!-- title row -->
    <div class="row" dir="rtl">

  <div class="col-3 algnC">
   
  </div>
  <div class="col-6">
  <h3  class="algnC titl">
  <img class="imgS" src="../assets/images/<?php echo $pic ?>" >
        
    </div>
    <div class="col-3 algnC">
    
    </div>

      <!-- /.col -->
    </div>

<div class="row rowss" >

<div class="col-12 algn">
  <div class="row">
<div class="col-3">
<span><b>رقم الفاتورة : </b></span>
</div>
<div class="col-5" style="margin-right: -20px;">
<span><b><?php echo $row2['samm_code']; ?></b> </span> 
</div>
  </div>

</div>
<div class="col-12 algn">
<b>التاريخ : </b><?php echo $row2['bill_date']; ?>  
</div>
<div class="col-12">
<span><b> اسم العميل : </b></span><span><?php echo $row2['customer_name']; ?> </span> 
</div>
<!-- <div class="col-12">
<span><b>  جوال العميل : </b></span><span><?php echo $row2['Mobile']; ?> </span> 
</div> -->

<!-- <div class="col-6 algn">
<span><b> العنوان الوطني : </b></span> <span> <?php echo $_SESSION['address']; ?></span> 
</div>
<div class="col-6">8410885090423
  8410885090423
  6222012400140

<span><b>  عنوان العميل : </b></span><span><?php echo $row2['customer_address']; ?></span> 
</div> -->

<div class="col-12 algn"> 
 <span><b>  الرقم الضريبى :</b></span><span> <?php echo $_SESSION['taxNum']; ?></span>
</div>
<!-- <div class="col-6">
<span><b>   الرقم الضريبى للعميل :</b></span><span> <?php echo $row2['customer_TaxNum']; ?></span>
</div>
<?php IF($row2['iban']){  ?>
<div class="col-12 algn"> 
 <span><b>  رقم الحساب  :</b></span><span><b>IBAN  :</b></span><span> <?php echo $row2['iban']; ?></span>
</div>
<?php } ?>
</div> -->

    <!-- info row -->
    <!-- <div class="row invoice-info" style="text-align: end">
      <div class="col-sm-7 invoice-col">
    
          <p> <span><b> اسم العميل : </b></span> <span> <?php echo $row2['customer_name']; ?></span> </p>
          <p> <span> <?php echo $row2['Mobile']; ?></span> <span><b> : الجوال </b></span></p>
       
      </div>

      <div class="col-sm-4 invoice-col" style="margin-top: 20px;">
        <?php echo $row2['id']; ?>  <b>: رقم الفاتورة</b><br>
        <b>التاريخ : </b><?php echo $row2['bill_date']; ?>  <br>
        <p><span><b> العنوان : </b></span> <span> <?php echo $_SESSION['address']; ?></span> </p>
        <p> <span> <?php echo $_SESSION['taxNum']; ?></span> <span><b> : الرقم الضريبى </b></span></p>
      </div>
    

    </div> -->
    <!-- /.row -->

    <!-- Table row -->
    <div class="row" style="margin:0px;direction: rtl;text-align: start;width: 100%;">
      <!-- <div class="col-1">
      </div> -->
      <div class="col-12 table">
        <table class="table table-striped" style="margin-top: 20px;margin-bottom: 0px;">
          <thead>
          <tr class="backG" style="text-align: center;">
            <th>م</th>
            <th>الصنف - الخدمة</th>
            <th>السعر</th>
            <th>الكمية</th>
            <th>المجموع</th>
          </tr>
          </thead>
          <tbody>
          <?php    while ($row3=mysqli_fetch_array($result3)) {     ?>
            <tr>
              <td><?php echo $i++; ?></td>

                <td><?php echo $row3['item']; ?></td>
                <td><?php echo $row3['price']; ?> </td>
               <td><?php echo $row3['amount']; ?></td>
               <td><?php echo $row3['total']; ?> </td>

            </tr>
            <?php }; ?>
          </tbody>
        </table>
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->
    <!-- <div class="col-1">
      </div> -->
    <div class="row" style="text-align: end; direction: rtl;">
       <!-- <div class="col-1">
      </div> -->

      <div class="col-12" style="    text-align: start;">
        <p class="lead" ></p>

        <div class="table">
         <table class="table">
            <tr>
              <th style="width:56%"> اجمالى  قيمة المبيعات :</th>
              <td><?php echo $row2['sum1']; ?> <span>ريال</span></td>
            </tr>
            <tr>
              <th>  التخفيض :</th>
              <td> <?php echo $row2['discount']; ?> <span>ريال</span></td>
            </tr>
            <?php if($_SESSION['vat']){  ?>
            <tr>
            <th> ضريبة القيمة المضافة :</th>
              <td><?php echo $row2['vat']; ?> <span>ريال</span></td>
            </tr>
            <?php } ?>
            <tr>
              <th> المبلغ النهائي :</th>
              <td><?php echo $row2['total']; ?> <span>ريال</span></td>
            </tr>
            <tr>
              <td colspan="2"><b><?php
              $ar_number= new convert_ar($row2['total'], "male");
              echo $ar_number->convert_number(). ' ريال سعودى فقط لا غير'; ?></b></td>
            </tr>
            <tr  >

            </tr>


          </table>
          <?php if ($row2['note']){  ?>
        <div>
          <div>ملاحظات :</div>
          <div>
          <?php echo $row2['note']; ?>
          </div>
        </div>
        <?php }?>
        </div>

      </div>
      <div class="col-sm-12">
              <div class="row">
              <div class="col-sm-4 algnC">
        <div style="margin-top: 25px;">
        
        <?php echo $generator->render_svg('qr', $urlBill, $options)?>
        </div>
            </div>
            <!-- <div class="col-sm-8 algnC">
              
            </div> -->
          <!-- <div class="col-sm-4 algnC">
            <div>الختم</div>
            <?php if($result44[17]){ ?>
            <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[16]; ?>" style="width: 180px;"></div>
           <?php } ?>
          </div>

            <div class="col-sm-4 algnC">
              <div>التوقيع</div>
              <?php if($result44[17]){ ?>
              <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[15]; ?>" style="width: 180px;"></div>
              <?php } ?>
            </div>       -->
        </div>
      </div>

      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->

    <!-- /.row -->
    <div class="col-1">
      </div>
    <div class="row" style="text-align: end; direction: rtl;">
       <div class="col-1">
      </div>

      <div class="col-10" style="    text-align: start;">
        <p class="lead" ></p>

        <div class="table-responsive">
          <table class="table" style="text-align: center;">
            <tr>
            <!-- <?php    while ($row4=mysqli_fetch_array($result4)) {     ?>
              <td style="width:50%;border: none;"><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $row4['sig_pic']; ?>" style="width: 180px;"></td>
            <?php } ?>  -->

            </tr>
            <tr style="padding-top: 80px;width:100%;">
           
            </tr>

          </table>
        </div>
      </div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</div>
<!-- ./wrapper -->

<script type="text/javascript">
  window.addEventListener("load", window.print());
</script>
</body>
</html>

