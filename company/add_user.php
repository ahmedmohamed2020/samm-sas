<?php
session_start();
include("../conn.php");


if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}

$company_id=$_SESSION['company_id'];
if(isset($_POST['save']))
{


$pic = $_FILES['thumb']['name'];
$target_dir = "../assets/images/";
$target_file = $target_dir.basename($_FILES['thumb']['name']);
move_uploaded_file($_FILES['thumb']['tmp_name'], $target_dir.$pic);



    
$emp_name=$_POST['emp_name'];
$emp_phone=$_POST['emp_phone'];
$job=$_POST['job'];
$emp_username=$_POST['emp_username'];
$emp_password=$_POST['emp_password'];;



 $sql="INSERT INTO emp(emp_name,emp_phone,emp_job,emp_username,emp_password,emp_pic,company_id)VALUES('$emp_name','$emp_phone','$job','$emp_username','$emp_password','$pic','$company_id')";
 mysqli_query($con,$sql); 
header("location:show_users.php");
 
}

?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

  </head>
  <body>
    <section class="body" style="direction: rtl;">

      <!-- start: header -->
      <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
         

          <!-- start: page -->
             <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
            
                <h3 class="panel-title"><i class="fa fa-plus" aria-hidden="true"></i> اضافة مستخدم</h3>
              </header>
            <div class="panel-body ">
                <form method="post" name="form-category-add" enctype="multipart/form-data" id="form-article-add">


        <div class="row">

            
            <div class="col-sm-8 col-xs-12">

               


            <div>
            <label>الاسم</label>
             <input type="text" name="emp_name"  class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>

            <div>
            <label>رقم الجوال</label>
             <input type="number" name="emp_phone"  class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>
                 

                    <div>
            <label>الوظيفة</label>
             <input type="text" name="job"  class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>
               
               
               
         

                     <div>
            <label>اسم المستخدم</label>
             <input type="text" name="emp_username"  class="form-control"
                
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>


                 <div>
            <label>كلمة المرور</label>
             <input type="password" name="emp_password"  class="form-control"
            
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                       
                />
                </div>

                

              </div>
              <div class="col-sm-4 col-xs-12 ">
                    <div class="box-infos-search">
                        <section class="content-header box-info-header">
                            <span class="content-title"> <i class="fa fa-image"></i> الصورة</span>
                            <a href="#" class="btn btn-default btn-search" onclick="TriggerInputFile('thumb',event);">
                                <i class="fa fa-search"></i>
                            </a>
                        </section>
                        <div class="box-infos  text-center">
                            <img class="thumb-preview img-responsive" src="../assets/images/no_image.jpg" style="width: 100%;height: 250px;">
                            <a href="#" class="badge thumb-reset img-responsive" onclick="resetThumb('thumb-preview',event);">Reset</a>
                              <input 
                              type="file"
                              class="form-control"
                              id="thumb" 
                              name="thumb" 
                        data-validation = ""
                        data-validation-error-msg = "عزراً ... الصورة ضرورية , ويجب أن تكون من النوع jpg , و الحجم لا يزيد عن 1M ! "
                        onchange="readUrl(this);" 
                />
                            
                        </div>
                    </div>
                </div>

               
                <div class="col-sm-12 col-xs-12">
                <hr>

                <div class="form-group text-center">
                    <button type="submit" id="btn-svae-category" class="btn btn-warning" name="save">حفظ</button>
                </div>
            </div>
        </div>
    </form>
              </div>
          </div>
          <!-- page end-->
        </div>
            </section>

            
           
      </div>

    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>


    <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../assets/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
 
  </body>
</html>