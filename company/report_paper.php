<?php
session_start();
include("../conn.php");

if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}


$sql3="SELECT *FROM  info where id =".$_SESSION['company_id'];
$result3 = mysqli_query($con,$sql3);
$row3=mysqli_fetch_array($result3);



$company_id=$_SESSION['company_id'];

$sql="SELECT * from  papers  where company_id='$company_id' order by paper_type";
$result=mysqli_query($con,$sql);


$x=0;

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>تقرير</title>
  <!-- Tell the browser to be responsive to screen width -->
  

    <!-- Head Libs -->

 <link rel="stylesheet" href="../assets/stylesheets/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../assets/stylesheets/adminlte.min.css">
</head>
<body style="padding-top: 50px">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-1">
      </div>
      <div class="col-10" style="text-align: right;">
       <div  class="row">
         <div  class="col-6">
         <img class="" style="width: 200px;height: 72px;float: left;" src="../assets/images/<?php echo $row3['pic']; ?>" >
        </div>
         <div  class="col-6">
            <h3 class="page-header" style="font-weight: bold;">
         <?php echo $row3['name']; ?></h3>
       <b> العنوان : </b> <?php echo $row3['address']; ?><br>
       <?php echo $row3['email']; ?><b> :  الايميل </b>  <br>
        <b> الهاتف </b> : <?php echo $row3['tel']; ?><br>
         </div>
         
       </div>
      
      </div>

        <div class="col-1">
      </div>
      <!-- /.col -->
    </div>
      <hr>
    <!-- info row -->
   <!-- info row -->
    <div class="row invoice-info" style="text-align: end">
      <div class="col-sm-4 invoice-col">
        
        <address>
      
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-3 invoice-col">
     
        <address>
         
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
       
      
        <b> تقرير  السندات <br>
       
      </div>
      <!-- /.col -->
      
    </div>
    <!-- /.row -->


    <!-- Table row -->
    <div class="row" style="direction: rtl;text-align: start;">
      <div class="col-1">
      </div>
      <div class="col-10 table-responsive">
        <table class="table table-striped">
          <thead>
         <tr>
              <th>#</th>
              <th>التاريخ</th>
                      <th>نوع السند</th>
                      <th>تصنيف السند</th>
                      <th>اسم العميل</th>
                      <th>المبلغ</th>
          </tr>
          </thead>
          <tbody>
             <?php  $i=1;  while ($row=mysqli_fetch_array($result)) {     ?>
                    <tr class="gradeX">
                      <td style="width: 15px"><?php echo $i++; ?></td>
                <td><?php echo $row['paper_date']; ?></td>
                      <td><?php echo $row['paper_type']; ?></td>
                      <td><?php echo $row['paper_cat']; ?></td>
                      <td><?php echo $row['paper_customer']; ?></td>
                      <td><?php echo $row['paper_amount'];$x=$x+$row['paper_amount']; ?></td>
                   
                    </tr>

                       <?php    }  ?>
            <tr>
              <td></td>
              <td>المجموع </td>
              <td></td>
              <td></td>
              <td></td>
              <td><?php echo $x; ?></td>
           
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->
   
      <!-- /.col -->
        <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>

