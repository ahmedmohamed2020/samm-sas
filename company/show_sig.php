<?php
include("../conn.php");


session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];
$sql="SELECT * from info where id='$company_id'";
$result=mysqli_query($con,$sql);
$result1=mysqli_fetch_row($result);

if($_SERVER['REQUEST_METHOD'] === 'POST')
{
$showSig1 = $_POST['check'];
$sql="UPDATE info set showSig = '$showSig1' WHERE id =".$company_id;
 mysqli_query($con,$sql);
 $sql="SELECT * from info where id='$company_id'";
 $result=mysqli_query($con,$sql);
 $result1=mysqli_fetch_row($result);
}

?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

    <link href="../assets/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="../assets/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="../assets/advanced-datatable/css/DT_bootstrap.css" />

  </head>
  <body>
    <section class="body">

      <!-- start: header -->
      <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
        

          <!-- start: page -->
            <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
                <div>
                   
                </div>
            
                <h3 class="panel-title"><i class="fa fa-users" aria-hidden="true"></i>التوقيع و الختم</h3>
              </header>
              <div class="row" style="padding-top: 50px">
              <div class="col-12" style="margin: 0 20px;">
              <form method="post" name="form-category-add" enctype="multipart/form-data" id="form-article-add">
              <input onChange="this.form.submit()" name="check" value="check" class="form-check-input" type="checkbox"  style="transform: scale(2.0);" <?php echo ($result1[17]== 'check' ? 'checked' : '');?> >
              <label class="form-check-label" for="flexCheckDefault" style="margin-right: 5px;">
               إظهار التوقيع والختم فى التقارير
              </label>
              </form>
              </div>

        
    <div class="col-sm-6 col-xs-12">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                       <?php if($result1[15]){ ?>
                                        <img style="height: 250px;width: 100%;" src="../assets/images/<?php echo $result1[15]; ?>" alt="" >
                                       <?php  }else{?>
                                        <img style="height: 250px;width: 100%;" src="../assets/images/sig.png" alt="" >
                                        <?php  }?>
                                        <a href="edit_sig.php?id=<?php echo $result1[0]; ?>"  class="btn btn-danger add-to-cart ">  <i class="fa fa-remove"></i> تعديل</a>
                                    </div>
                                    <div class="product-overlay">
                                     
                                    </div>
                                </div>
                                <div class="choose">
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                    <?php if($result1[16]){ ?>
                                        <img style="height: 250px;width: 100%;" src="../assets/images/<?php echo $result1[16]; ?>" alt="" >
                                        <?php  }else{?>
                                          <img style="height: 250px;width: 100%;" src="../assets/images/logoPrint.png" alt="" >
                                        <?php  } ?>             
                                        <a href="edit_logo.php?id=<?php echo $result1[0]; ?>"  class="btn btn-danger add-to-cart ">  <i class="fa fa-remove"></i> تعديل</a>

                                    </div>
                                    <div class="product-overlay">
                                     
                                    </div>
                                </div>
                                <div class="choose">
                                    
                                </div>
                            </div>
                        </div>
                </div>


            </section>

            
           
      </div>

     
   
   
   
    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>

  </body>
</html>