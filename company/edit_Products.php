<?php
include("../conn.php");


session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];

$sql12="SELECT *FROM  producats WHERE id =".$_GET['id'];
$result1 = mysqli_query($con,$sql12);
$row1=mysqli_fetch_array($result1);

$sql="SELECT * from category where company_id='$company_id'";
$catg=mysqli_query($con,$sql);
$sql1="SELECT * from types where company_id='$company_id'";
$typs=mysqli_query($con,$sql1);
$sql2="SELECT * from mainuom where company_id='$company_id'";
$mainU=mysqli_query($con,$sql2);
$sql3="SELECT * from subuom where company_id='$company_id'";
$sumU=mysqli_query($con,$sql3);


if(isset($_POST['save']))
{    
$item_name=$_POST['item_name'];
$item_barcode=$_POST['item_barcode'];
$item_cat=$_POST['item_cat'];
$item_type=$_POST['item_type'];
$item_mainUom=$_POST['item_mainUom'];
$item_subUom=$_POST['item_subUom'];
$specs=$_POST['specs'];
$price=$_POST['price'];
$amount=$_POST['amount'];
$note=$_POST['note'];
$pic = $_FILES['thumb']['name'];
$target_dir = "../assets/images/";

$target_file = $target_dir.basename($_FILES['thumb']['name']);
move_uploaded_file($_FILES['thumb']['tmp_name'], $target_dir.$pic);
$sql="UPDATE producats SET item_name='$item_name',item_barcode='$item_barcode',item_cat='$item_cat',item_type='$item_type',item_mainUom='$item_mainUom',item_subUom='$item_subUom',specs='$specs',price='$price',amount='$amount',note='$note',pic='$pic' WHERE id =".$_GET['id'];

$chk = mysqli_query($con,$sql)or die(mysqli_error($con));

if($chk){
header("location:show_Products.php");
}
else{
  echo('Error');
}
}
?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

    <link href="../assets/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="../assets/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="../assets/advanced-datatable/css/DT_bootstrap.css" />

  </head>
  <body>
    <section class="body">

      <!-- start: header -->
     <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
         

          <!-- start: page -->
            <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
            
                <h3 class="panel-title"><i class="fa fa-plus" aria-hidden="true"></i> اضافة صنف</h3>
              </header>
              <div class="panel-body ">
               <form method="post" name="form-category-add" enctype="multipart/form-data" id="form-article-add">


        <div class="row">

            
            <div class="col-sm-8 col-xs-12">
            <div  class="col-sm-12 col-xs-12">
            <label>اسم الصنف</label>
             <input type="text" name="item_name" value="<?php echo $row1['item_name']; ?>"  class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>
            <div class="col-sm-12 col-xs-12">
            <label>الباركود</label>
             <input type="text" name="item_barcode" value="<?php echo $row1['item_barcode']; ?>"  class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "
                />
                </div>  
                <div class="col-sm-12 col-xs-12">
<div>
  <label> الفئات</label>
</div>
            
            <select class="custom-select" name="item_cat"  id="item_cat">
            <option value="" ></option>
            <?php    while ($row=mysqli_fetch_array($catg)) {     ?>
            <option <?= $row1['item_cat'] ==  $row['cat_name']? ' selected="selected"' : '';?> value="<?php echo $row['cat_name']; ?>"><?php echo $row['cat_name']; ?></option>
            <?php    }  ?>
            </select>
                </div>
                <div class="col-sm-12 col-xs-12">
                  <div>
                    <label> الانواع</label>
                  </div>
            
            <select class="custom-select" name="item_type"  id="item_type">
            <option value=""></option>
            <?php    while ($row=mysqli_fetch_array($typs)) {     ?>
            <option <?= $row1['item_type'] ==  $row['type_name']? ' selected="selected"' : '';?> value="<?php echo $row['type_name']; ?>"><?php echo $row['type_name']; ?></option>
            <?php    }  ?>
            </select>
                </div>
                <div class="col-sm-12 col-xs-12">
                  <div>
                   <label>الوحدات الرئيسية</label> 
                  </div>
            
            <select class="custom-select" name="item_mainUom"  id="item_mainUom">
            <option value=""></option>
            <?php    while ($row=mysqli_fetch_array($mainU)) {     ?>
            <option <?= $row1['item_mainUom'] ==  $row['uom_name']? ' selected="selected"' : '';?> value="<?php echo $row['uom_name']; ?>"><?php echo $row['uom_name']; ?></option>
            <?php    }  ?>
            </select>
                </div>
                <div class="col-sm-12 col-xs-12">
                  <div>
                  <label>الوحدات الفرعية</label>  
                  </div>
            
            <select class="custom-select" name="item_subUom"  id="item_subUom">
            <option value=""></option>
            <?php    while ($row=mysqli_fetch_array($sumU)) {     ?>
            <option <?= $row1['item_subUom'] ==  $row['suom_name']? ' selected="selected"' : '';?> value="<?php echo $row['suom_name']; ?>"><?php echo $row['suom_name']; ?></option>
            <?php    }  ?>
            </select>
                </div>



                 <div class="col-sm-12 col-xs-12">
            <label>الكمية</label>
             <input type="text" name="amount" value="<?php echo $row1['amount']; ?>" class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "              
                />
                </div>
                <div class="col-sm-12 col-xs-12">
            <label>السعر</label>
             <input type="text" name="price" value="<?php echo $row1['price']; ?>" class="form-control"
                        data-validation = "required"
                        data-validation-length = "",
                        data-validation-error-msg = "عزراً ... لايمكن ترك حقل فارغ "              
                />
                </div>
                <div class="col-sm-12 col-xs-12">
                      <div>
                    <label>المواصفات</label>     
                      </div>
            <textarea  name="specs"  class="form-control">
            <?php echo $row1['specs']; ?>
               </textarea>
                </div>
                <div class="col-sm-12 col-xs-12">
                 <label>ملاحظات</label>
             <textarea  name="note"  class="form-control">
             <?php echo $row1['note']; ?>
             </textarea>
                </div>
              </div>
              <div class="col-sm-4 col-xs-12">
                    <div class="box-infos-search">
                        <section class="content-header box-info-header">
                            <span class="content-title"> <i class="fa fa-image"></i> صورة الصنف </span>
                            <a href="#" class="btn btn-default btn-search" onclick="TriggerInputFile('thumb',event);">
                                <i class="fa fa-search"></i>
                            </a>
                        </section>
                        <div class="box-infos  text-center">
                          <?php if($row1['pic']){ ?>
                            <img class="thumb-preview img-responsive" src="../assets/images/<?php echo $row1['pic']; ?>" style="width: 100%;height: 250px;">
                         <?php } else {?>
                            <img class="thumb-preview img-responsive" src="../assets/images/no_image.jpg" style="width: 100%;height: 250px;">
                          <?php }?>
                            <a href="#" class="badge thumb-reset img-responsive" onclick="resetThumb('thumb-preview',event);">Reset</a>
                              <input 
                              type="file"
                              class="form-control"
                              id="thumb" 
                              name="thumb" 
                        data-validation = ""
                        data-validation-error-msg = "عزراً ... الصورة ضرورية , ويجب أن تكون من النوع jpg , و الحجم لا يزيد عن 1M ! "
                        onchange="readUrl(this);" 
                />
                            
                        </div>
                    </div>
                </div>

               
                <div class="col-sm-12 col-xs-12">
                <hr>

                <div class="form-group text-center">
                    <button type="submit" id="btn-svae-category" class="btn btn-warning" name="save">حفظ</button>
                </div>
            </div>
        </div>
    </form>
              </div>
          </div>
          <!-- page end-->
        </div>
        
            </section>

            
           
      </div>

    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>
  </body>
</html>