
<?php 


include("../conn.php");
session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];


$sql1="SELECT *FROM  info ";
$result1 = mysqli_query($con,$sql1);
$row1=mysqli_fetch_array($result1);

$sql2="SELECT *FROM  carts,emp,customer where customer.customer_id=carts.carts_cus and emp_id=carts_emp_id and carts_id= ".$_GET['id'];
$result2 = mysqli_query($con,$sql2);
$row2=mysqli_fetch_array($result2);

$sql4="SELECT *FROM  sig where company_id=".$_SESSION['company_id'];
$result4 = mysqli_query($con,$sql4);

$sql3="SELECT * from services,cart1 where  services.service_id=cart1.item_id and carts_id= ".$_GET['id'];
$result3=mysqli_query($con,$sql3);
$i=1;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ARBAB MARKA</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css2/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css2/adminlte.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body>
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-1">
      </div>
      <div class="col-10" style="text-align: right;">
        <h3 class="page-header" style="float: right;">فاتورة خدمات
          </h3>
        
      </div>
        <div class="col-1">
      </div>
      <!-- /.col -->
    </div>
    
    <!-- info row -->
    <div class="row invoice-info" style="text-align: end">
      <div class="col-sm-4 invoice-col">
        
        <address>
          <strong><?php echo $row2['carts_id']; ?>  <b>: رقم الفاتورة</b><br></strong>
          <b>التاريخ : </b><?php echo $row2['carts_date']; ?>  <br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-3 invoice-col">
         
        <address>
          <strong></strong><br>
          
        
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
       
        <?php echo $row1['name']; ?><br>
        <b> العنوان : </b><?php echo $row1['address']; ?><br>
          <b>الجوال  : </b><?php echo $row1['tel']; ?><br>
          <b>الرقم الضريبي : </b><?php echo $row2['customer_tax']; ?><br>
          <b>السيد : </b><?php echo $row2['customer_name']; ?><br>
      </div>
      <!-- /.col -->
      
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row" style="direction: rtl;text-align: start;">
      <div class="col-1">
      </div>
      <div class="col-10 table-responsive">
         <table class="table table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>الخدمة</th>
            <th>السعر</th>
            <th>الكمية</th>
            <th>المبلغ</th>
          </tr>
          </thead>
          <tbody>
          <?php    while ($row3=mysqli_fetch_array($result3)) {     ?>
            <tr>
              <td><?php echo $i++; ?></td>
              
                <td><?php echo $row3['service_name']; ?></td>
               <td><?php echo $row3['service_price']; ?></td>
               <td><?php echo $row3['quan']; ?></td>
               <td ><?php echo $row3['service_price'] * $row3['quan']; ?> </td>
             
            </tr>
            <?php }; ?>
          </tbody>
        </table>
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->
    <div class="col-1">
      </div>
    <div class="row" style="text-align: end; direction: rtl;">
       <div class="col-1">
      </div>
 
      <div class="col-6" style="    text-align: start;">
        <p class="lead" ></p>

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:56%"> اجمالى  قيمة المبيعات :</th>
              <td><?php echo $row2['carts_price']; ?></td>
            </tr>
            <tr>
              <th>  التخفيض :</th>
              <td> <?php echo $row2['carts_discount']; ?></td>
            </tr>
            <tr>
              <th> ضريبة القيمة المضافة(15%) :</th>
              <td><?php echo $row2['carts_tax']; ?></td>
            </tr>
            <tr>
              <th> المبلغ النهائي :</th>
              <td><?php echo $row2['carts_end_price']; ?></td>
            </tr>
            <tr  style="height: 200px">
             
            </tr>
         
           
          </table>
        </div>
      </div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->
     
    <!-- /.row -->
    <div class="col-1">
      </div>
    <div class="row" style="text-align: end; direction: rtl;">
       <div class="col-1">
      </div>
 
      <div class="col-10" style="    text-align: start;">
        <p class="lead" ></p>

        <div class="table-responsive">
          <table class="table" style="text-align: center;">
            <tr>
              <?php    while ($row4=mysqli_fetch_array($result4)) {     ?>
              <td style="width:50%;border: none;"><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $row4['sig_pic']; ?>" style="width: 180px;"></td>
            <?php } ?>
        
            </tr>
            <tr style="padding-top: 80px;width:100%;">
                  <td style="border: none;"><img class="thumb-preview img-responsive" src="../assets/images/qr.png" style="width: 80px;"></td>
            </tr>
            
          </table>
        </div>
      </div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->

<script type="text/javascript"> 
  window.addEventListener("load", window.print());
</script>
</body>
</html>

