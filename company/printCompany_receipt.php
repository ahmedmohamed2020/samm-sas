
<?php

include("../conn.php");
include("../barcode.php");
include("../num_to_ar.php");
session_start();
$generator = new barcode_generator();
$options = array();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];

$pic=$_SESSION['Cpic'];

$dateFrom=$_SESSION['dateFrom'];
$dateTo=$_SESSION['dateTo'];
// $sql1="SELECT *FROM  info ";
// $result1 = mysqli_query($con,$sql1);
// $row1=mysqli_fetch_array($result1);

// $sql2="SELECT *FROM  carts,emp,customer where customer.customer_id=carts.carts_cus and emp_id=carts_emp_id and carts_id= ".$_GET['id'];
// $result2 = mysqli_query($con,$sql2);
// $row2=mysqli_fetch_array($result2);
$sql="SELECT * FROM  papers where paper_type='سند قبض' and company_id=".$company_id ." and paper_date >= '".$dateFrom."' and paper_date < '".$dateTo."'";
$result = mysqli_query($con,$sql);

// $urlBill = $serverName . 'printBillHeat.php?token=\'' . $row2['token'].'\'';
$sql4="SELECT * FROM  info where id=".$_SESSION['company_id'];
$result4 = mysqli_query($con,$sql4);
$result44=mysqli_fetch_row($result4);

$i=1;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> تقرير سندات القبض</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css2/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css2/adminlte.min.css">
  <link rel="stylesheet" href="../css2/print.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .rowss{
      direction: rtl;
      text-align: right;
      margin: 16px;
    }
    .algn{
      text-align: right;
    }
    .algnC{
      text-align: center;
    }
    .centerdv{
      display: flex;
    justify-content: center;
    }
    .titl{
      text-decoration: underline;
    font-size: 22px;
    }
    .backG{
      background-color: #d0d0d0;
    }
    .mr{
      margin-top: 32px;
    }
    .imgS{
      width:180px;
      height: 86px;
    }
    .imgSQR{
      width:235px;
      height: 235px;
    }
    .brd{
      border: 1px solid;
      line-height: 36px;
    height: 44px;
    }
    .bgclr{
      background-color: #e3e3e3;
    }
    .logod{
      font-size: 24px;
    line-height: 10vh;
    font-weight: bold;
    }
    .bld{
      font-size: 15px;
      font-weight: bolder;
    }
    thead{
      background-color: #dddddd;
      text-align: center;
    }
    .bck{
      background-color: #ddd;
    }
  </style>
</head>
<body>
  <div class="page2">

<div class="">
  <!-- Main content -->
  <section >
    <!-- title row -->
    <div class="row" dir="rtl">

  <div class="col-6 algnC logod">
  <?php echo $_SESSION['company']; ?>  </h3>
  </div>
  <div class="col-3">
  <h3  class="algnC titl">
       
        
    </div>
    <div class="col-3 algnC">
    <img class="imgS" src="../assets/images/<?php echo $pic ?>" >
    </div>

      <!-- /.col -->
    </div>
<div class="row" style="font-weight: bolder;">
<div class="col-4">

</div>
<div class="col-4" style="text-align: center;font-size: 20px;">
تقرير سندات القبض عن فترة
</div>
<div class="col-4">

</div>
<div class="col-4">

</div>
<div class="col-4" style="text-align: center;font-size: 16px;">
<span>
  التاريخ من /
</span>
<span>
<?php echo $dateFrom; ?>  </h3>
</span>
<span>
  الى /
</span>
<span>
<?php echo $dateTo; ?>  </h3>
</span>
</div>
<div class="col-4">

</div>
</div>
<div class="row rowss" style="margin: 17px 85px;text-align: center;">
<table class="table">
<thead>
  <tr>
    <th>
      م
    </th>
    <th>
      الكود
    </th>
    <th>
      اسم العميل 
    </th>
    <th>
     التاريخ
    </th>
    <th>
     طريقة الدفع
    </th>
    <th>
     الغرض
    </th>
    <th>
     الاجمالى
    </th>
  </tr>
</thead>
<tbody>
<?php  $i=1; $sumT=0;  while ($row=mysqli_fetch_array($result)) {     ?>
  <tr>
  <td><?php echo $i++; ?></td>
  <td><?php echo $row['paper_id']; ?></td>
  <td><?php echo $row['paper_customer']; ?></td>
  <td><?php echo $row['paper_date']; ?></td>
  <td><?php echo $row['pay_type']; ?></td>
  <td><?php echo $row['payfor']; ?></td>
  <td><?php echo $row['paper_amount']; ?></td>
  </tr>
  <?php $sumT = $sumT + $row['paper_amount'];   }  ?>
  <tr>
    <td colspan="6" class="bck" style="text-align: center;font-weight: bold;">
      الاجمالى
    </td>
    <td class="bck">
    
    <span style="font-weight: bold;font-size: 15px;"><?php echo $sumT; ?></span>
    <span>ريال</span>
    </td>
  </tr>
</tbody>
</table>
</div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <div class="col-sm-12">
              <div class="row">
              <div class="col-sm-4 algnC">
            <div>الختم</div>
            <?php if($result44[17]){ ?>
            <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[16]; ?>" style="width: 180px;"></div>
           <?php } ?>
          </div>

            <div class="col-sm-4 algnC">
              <div>التوقيع</div>
              <?php if($result44[17]){ ?>
              <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[15]; ?>" style="width: 180px;"></div>
              <?php } ?>
            </div>  
              <div class="col-sm-4 algnC">
        <div>
        

        </div>
            </div>
    
        </div>
      </div>
    <!-- /.row -->

    <!-- /.row -->
    <div class="col-1">
      </div>
    <div class="row" style="text-align: end; direction: rtl;">
       <div class="col-1">
      </div>
      <div class="col-10" style="    text-align: start;">
        <p class="lead" ></p>
        <div class="table-responsive">
          <table class="table" style="text-align: center;">
            <tr>
            <!-- <?php    while ($row4=mysqli_fetch_array($result4)) {     ?>
              <td style="width:50%;border: none;"><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $row4['sig_pic']; ?>" style="width: 180px;"></td>
            <?php } ?>  -->

            </tr>
            <tr style="padding-top: 80px;width:100%;">
           
            </tr>

          </table>
        </div>
      </div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</div>
<!-- ./wrapper -->

<script type="text/javascript">
   window.addEventListener("load", window.print());
</script>
</body>
</html>

