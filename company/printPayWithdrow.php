
<?php

include("../conn.php");
include("../barcode.php");
include("../num_to_ar.php");
session_start();
$generator = new barcode_generator();
$options = array();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];

$Bill_id=$_GET['id'];
$pic=$_SESSION['Cpic'];
// $sql1="SELECT *FROM  info ";
// $result1 = mysqli_query($con,$sql1);
// $row1=mysqli_fetch_array($result1);

// $sql2="SELECT *FROM  carts,emp,customer where customer.customer_id=carts.carts_cus and emp_id=carts_emp_id and carts_id= ".$_GET['id'];
// $result2 = mysqli_query($con,$sql2);
// $row2=mysqli_fetch_array($result2);
$sql4="SELECT * FROM  papers where paper_id=".$Bill_id;
$result4 = mysqli_query($con,$sql4);
$row2=mysqli_fetch_array($result4);
// $urlBill = $serverName . 'printBillHeat.php?token=\'' . $row2['token'].'\'';
$sql4="SELECT * FROM  info where id=".$_SESSION['company_id'];
$result4 = mysqli_query($con,$sql4);
$result44=mysqli_fetch_row($result4);

$i=1;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> سند صرف</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css2/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css2/adminlte.min.css">
  <link rel="stylesheet" href="../css2/print.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .rowss{
      direction: rtl;
      text-align: right;
      margin: 16px;
    }
    .algn{
      text-align: right;
    }
    .algnC{
      text-align: center;
    }
    .centerdv{
      display: flex;
    justify-content: center;
    }
    .titl{
      text-decoration: underline;
    font-size: 22px;
    }
    .backG{
      background-color: #d0d0d0;
    }
    .mr{
      margin-top: 32px;
    }
    .imgS{
      width:180px;
      height: 86px;
    }
    .imgSQR{
      width:235px;
      height: 235px;
    }
    .brd{
      border: 1px solid;
      line-height: 36px;
    height: 44px;
    }
    .bgclr{
      background-color: #e3e3e3;
    }
    .logod{
      font-size: 24px;
    line-height: 10vh;
    font-weight: bold;
    }
    .bld{
      font-size: 15px;
      font-weight: bolder;
    }
  </style>
</head>
<body>
  <div class="page2">

<div class="">
  <!-- Main content -->
  <section >
    <!-- title row -->
    <div class="row" dir="rtl">

  <div class="col-6 algnC logod">
  <?php echo $_SESSION['company']; ?>  </h3>
  </div>
  <div class="col-3">
  <h3  class="algnC titl">
       
        
    </div>
    <div class="col-3 algnC">
    <img class="imgS" src="../assets/images/<?php echo $pic ?>" >
    </div>

      <!-- /.col -->
    </div>

<div class="row rowss" style="margin: 17px 85px;text-align: center;">

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
  <div class="row" style="margin: 20px;width: 100%;">
<div class="col-6 bgclr brd">
رقم السند
</div>
<div class="col-6 brd">
  <?php echo $row2["paper_id"]; ?>
</div>
<div class="col-6 bgclr brd">
التاريخ
</div>
<div class="col-6 brd">
  <span><?php echo $row2["paper_date"]; ?></span>

</div>
  </div>
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 " style="font-size: 32px;">
سند صرف
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
<div class="row" style="margin: 20px;">
<div class="col-12 bgclr brd">
المبلغ - Amount
</div>
<div class="col-12  brd" style="font-size: 22px;background-color: #b8ffe7;">
<span><?php echo $row2["paper_amount"]; ?></span><span> ريال </span>
</div>

  </div>
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
استلمت من السادة :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<?php echo $row2["paper_customer"]; ?>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Received From 
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
 مبلغ وقدره :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<b><?php
              $ar_number= new convert_ar($row2['paper_amount'], "female");
              echo $ar_number->convert_number(). ' ريال سعودى فقط لا غير'; ?></b>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Amount SR 
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
  وذلك مقابل :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<?php echo $row2["payfor"]; ?>

</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Payment against 
</div>


<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
   طريقة الدفع :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<?php echo $row2["pay_type"]; ?>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Payment Method 
</div>


<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
   شيك رقم :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<?php echo $row2["shek_num"]; ?>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Cheque No
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
    مسحوب على بنك :
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 brd">
<?php echo $row2["bank"]; ?>
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 brd bgclr">
: Bank
</div>



<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  bld">
      المحاسب 
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">

</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  bld">
 المستلم
</div>

<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  ">
<?php echo $row2["almohasel"]; ?>
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
</div>
<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  ">
<?php echo $row2["almostalem"]; ?>
</div>
</div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <div class="col-sm-12">
              <div class="row">
              <div class="col-sm-4 algnC">
            <div>الختم</div>
            <?php if($result44[17]){ ?>
            <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[16]; ?>" style="width: 180px;"></div>
           <?php } ?>
          </div>

            <div class="col-sm-4 algnC">
              <div>التوقيع</div>
              <?php if($result44[17]){ ?>
              <div><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $result44[15]; ?>" style="width: 180px;"></div>
              <?php } ?>
            </div>  
              <div class="col-sm-4 algnC">
        <div>
        

        </div>
            </div>
    
        </div>
      </div>
    <!-- /.row -->

    <!-- /.row -->
    <div class="col-1">
      </div>
    <div class="row" style="text-align: end; direction: rtl;">
       <div class="col-1">
      </div>
      <div class="col-10" style="    text-align: start;">
        <p class="lead" ></p>
        <div class="table-responsive">
          <table class="table" style="text-align: center;">
            <tr>
            <!-- <?php    while ($row4=mysqli_fetch_array($result4)) {     ?>
              <td style="width:50%;border: none;"><img class="thumb-preview img-responsive" src="../assets/images/<?php echo $row4['sig_pic']; ?>" style="width: 180px;"></td>
            <?php } ?>  -->

            </tr>
            <tr style="padding-top: 80px;width:100%;">
           
            </tr>

          </table>
        </div>
      </div>
      <!-- /.col -->
       <div class="col-1">
      </div>
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
</div>
<!-- ./wrapper -->

<script type="text/javascript">
  window.addEventListener("load", window.print());
</script>
</body>
</html>

