<?php
include("../conn.php");


session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id = $_SESSION['company_id'];
$sql = "SELECT * from producats where company_id='$company_id'";
$result = mysqli_query($con, $sql);

$sql1 = "SELECT * from services where company_id='$company_id'";
$result1 = mysqli_query($con, $sql1);

?>
<html class="fixed">

<head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

    <link href="../assets/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="../assets/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="../assets/advanced-datatable/css/DT_bootstrap.css" />
    <style>
        .lbl {
            font-size: 17px;
            font-weight: bold;
        }

        .num {
            color: #03ff04;
        }

        #datatable-default_wrapper {
            min-width: 100%;
        }

        .dataTables_wrapper .dataTables_filter input {
            margin-left: -127px;
            width: 200px;
        }

        .modal-header .close {
            margin-top: -22px;
        }

        .table {
            border: 1px solid #fff !important;
        }

        td,
        th {
            text-align: center !important;
        }

        .textAr {
            width: 380px !important;
        }

        .hei {
            margin: 5px auto;
        }
    </style>
</head>

<body>
    <section class="body">

        <!-- start: header -->
        <?php include("header.php"); ?>
        <!-- end: header -->

        <div class="inner-wrapper">
            <!-- start: sidebar -->
            <?php include("side.php"); ?>
            <!-- end: sidebar -->

            <section role="main" class="content-body">

                <div class="row">
                    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12 col-xl-12">
                        <section class="panel">
                            <header class="panel-heading">
                                <div class="panel-actions">
                                    <a href="#" class="fa fa-caret-down"></a>
                                    <a href="#" class="fa fa-times"></a>
                                </div>
                                <div>
                                <a id="print_billBarcode" class='btn btn-warning btn-xs ' style="float: left;"><i class="fa  fa-print" aria-hidden="true"></i> طباعة حرارية
                                    </a>
                                <a id="print_billcustom" class='btn btn-success btn-xs ' style="float: left;"><i class="fa  fa-print" aria-hidden="true"></i> طباعة - مخصصة
                                    </a>
                                <a id="print_billLetter" class='btn btn-info btn-xs ' style="float: left;"><i class="fa  fa-print" aria-hidden="true"></i> طباعة - ترويسة
                                    </a>
                                    <a id="print_bill" class='btn btn-warning btn-xs ' style="float: left;"><i class="fa  fa-print" aria-hidden="true"></i> طبــــاعة
                                    </a>
                                    <a id="save_bill" class='btn btn-success btn-xs ' style="float: left;"><i class="fa  fa-save" aria-hidden="true"></i> حفـــــــظ
                                    </a>
                                    <a id="k1" href="./Bills.php" class='btn btn-primary btn-xs ' style="float: left;"><i class="fa  fa-plus" aria-hidden="true"></i> فاتورة جديدة
                                    </a>
                                </div>
                                <h3 class="panel-title">المبيعات</h3>
                            </header>
                            <div class="panel-body table-responsive">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                        <label class="lbl">رقم الفاتورة</label>
                                        <label class="lbl"> : </label>
                                        <label id="billId" class="lbl num" style="display: none;"> </label>
                                        <label id="samm_code" class="lbl num"> </label>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 hei">
                                        <label class="lbl"> رقم الجوال </label>
                                        <label class="lbl"> : </label>
                                        <input id="Mobile" type="text" class="textAr">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 hei">
                                        <label class="lbl">اسم العميل </label>
                                        <label class="lbl"> : </label>
                                        <input id="customerName" type="text" class="textAr ">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 hei">
                                        <label class="lbl"> الرقم الضريبى </label>
                                        <label class="lbl"> : </label>
                                        <input id="customer_TaxNum" type="text" class="textAr">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6 hei">
                                        <label class="lbl">عنوان العميل </label>
                                        <label class="lbl"> : </label>
                                        <input id="customer_address" type="text" class="textAr ">
                                    </div>
                                    <!-- <div class="col-xs-12 col-sm-12 col-md-12 hei">
                                        <label class="lbl"> رقم الحساب (IBAN) </label>
                                        <label class="lbl"> : </label>
                                        <input id="iban" type="text" class="textAr ">
                                    </div> -->
                                    <div class="col-xs-12 col-sm-12 col-md-12 hei">
                                        <label class="lbl"> ملاحظات </label>
                                        <label class="lbl"> : </label>
                                        <textarea id="note" name="note" style="width: 100%;">
                                        </textarea>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label class="lbl">الاجمالى</label>
                                        <label class="lbl"> : </label>
                                        <label id="total" class="lbl num"> 0</label>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label class="lbl">الخصم</label>
                                        <label class="lbl "> : </label>
                                        <input id="discount" type="text" class="num" value="0">
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label class="lbl">الضريبة</label>
                                        <label class="lbl"> : </label>
                                        <label id="vat" class="lbl num"> 0</label>
                                        <label id="vatValue" style="display: none;" class="lbl num"> <?php echo $_SESSION['vat'];  ?></label>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-3">
                                        <label class="lbl">المجموع</label>
                                        <label class="lbl"> : </label>
                                        <label id="sum1" class="lbl num"> 0</label>
                                    </div>
                                    <div class="col-md-12">طريقة الدفع</div>

                                    <div class="col-md-4 lbl">
                                        شبكة
                                        <input name="pay_method" type="radio" value="network">
                                    </div>
                                    <div class="col-md-4 lbl">
                                        تحويل
                                        <input name="pay_method" type="radio" value="visa">
                                    </div>
                                    <div class="col-md-4 lbl">
                                        كاش
                                        <input name="pay_method" type="radio" checked value="cache">
                                    </div>
                                </div>

                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                   إضافة صنف - خدمة
                                </button>
                                <label class="lbl"> باركود </label>
                                        <label class="lbl"> : </label>
                                        <input id="barcodeID" type="text" class="textAr " oninput="addByBarcode()">
                                <table id="itemsT" class="table table-bordered table-striped mb-none dataTable no-footer">
                                    <thead style="    background: #34495e;">
                                        <tr role="row">
                                            <th>كود</th>
                                            <th style="width: 286px;">الصنف - الخدمة</th>
                                            <th>السعر</th>
                                            <th>الكمية</th>
                                            <th>الاجمالى</th>
                                            <th style="text-align: center;">حذف</th>
                                        </tr>
                                    </thead>
                                    <tbody id="myTable">


                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>

                </div>
                <!-- start: page -->

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <table class="table table-bordered table-striped mb-none" id="datatable-default" style="direction: rtl;">
                                    <thead style="    background: #34495e;">
                                        <tr>
                                            <th>كود</th>
                                            <th style="width: 101px">الصنف - الخدمة</th>
                                            <th>السعر</th>
                                            <th style="text-align: center;">....</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php while ($row = mysqli_fetch_array($result)) {     ?>
                                            <tr class="gradeX">
                                                <td><?php echo $row['id']; ?></td>

                                                <td style="width: 101px"><?php echo $row['item_name']; ?></td>
                                                <td><?php echo $row['price']; ?></td>
                                                <td style="text-align: center;">
                                                    <a onclick="addItem(<?php echo $row['id'] ?>)" class=" btn btn-warning btn-xs" name="<?php echo $row['id'] ?>" class='btn btn-warning btn-xs '><i class="fa fa-pencil" aria-hidden="true"></i> اختيار</i> </a>
                                                </td>
                                            </tr>
                                        <?php    }  ?>
                                        <?php while ($row = mysqli_fetch_array($result1)) {     ?>
                                            <tr class="gradeX">
                                                <td><?php echo $row['service_id']; ?></td>
                                                <td style="width: 101px"><?php echo $row['service_name']; ?></td>
                                                <td><?php echo $row['service_price']; ?></td>
                                                <td style="text-align: center;">
                                                    <a onclick="addItemServ(<?php echo $row['service_id'] ?>)" class=" btn btn-warning btn-xs" name="<?php echo $row['service_id'] ?>" class='btn btn-warning btn-xs '><i class="fa fa-pencil" aria-hidden="true"></i> اختيار</i> </a>
                                                </td>
                                            </tr>
                                        <?php    }  ?>

                                    </tbody>
                                </table>
                            </div>
                            <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
                        </div>
                    </div>
                </div>




            </section>
        </div>

        <!-- Vendor -->
        <script src="../assets/vendor/jquery/jquery.js"></script>
        <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
        <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
        <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
        <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
        <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

        <!-- Specific Page Vendor -->
        <script src="../assets/vendor/select2/select2.js"></script>
        <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
        <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
        <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>

        <!-- Theme Base, Components and Settings -->
        <script src="../assets/javascripts/theme.js"></script>

        <!-- Theme Custom -->
        <script src="../assets/javascripts/theme.custom.js"></script>

        <!-- Theme Initialization Files -->
        <script src="../assets/javascripts/theme.init.js"></script>


        <!-- Examples -->
        <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
        <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
        <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>

        <script type="text/javascript">
                function addByBarcode(){
                    var value = document.getElementById('barcodeID').value;
                    if (value.length > 5) {
                        const formData = {
                        'barcode': value
                         };
                        $.ajax({
                        url: "./getBillRowBarcode.php",
                        type: "post",
                        data: formData,
                        success: function(d) {
                            const lst = JSON.parse(d)
                            if (lst.status === 200) {
                            $('#myTable').append('<tr><td>' + lst.id + '</td><td style="width: 486px;">' + lst.item_name + '</td><td class="price">' + lst.price + '</td><td><input type="text" class="identifier" value="1" /></td><td class="total">' + lst.price + '</td><td><a class="rmv btn btn-danger btn-xs delete"supp_id="2"><i class="fa   fa-times" aria-hidden="true"></i>  حذف</i></a></td></tr>');
                            // table.row.add([lst.id,lst.item_barcode,lst.item_name,'<input type="text" />',lst.price]).draw();
                            const sum11 = $('#sum1').html()
                            const vat1 = $('#vat').html()
                            const discount11 = $("#discount").val();
                            const total1 = $('#total').html()
                            const vatValue = $('#vatValue').html()
                            let suum = 0
                            let vattt = 0
                            let totall555 = 0
                            if (sum1) {
                                suum = parseFloat(sum11);
                                suum = suum + parseFloat(lst.price)
                            } else {
                                suum = suum + parseFloat(lst.price)
                            }

                            if(vatValue){
                            vattt = (parseFloat(suum) * vatValue) / 100;
                             totall555 = parseFloat(suum) + parseFloat(vattt);
                            }
                            let discount12 = 0;
                            if (discount11) {
                                discount12 = parseFloat(discount11);
                            }
                            const totall5 = totall555 - discount12;
                            const summ1 = Math.round(suum) 
                            const vatt1 = Math.round(vattt) 
                            const totall55 = Math.round(totall5) 
                            $('#sum1').html(summ1)
                            $('#vat').html(vatt1)
                            $('#total').html(totall55)
                            document.getElementById('barcodeID').value = "";
                            document.getElementById('barcodeID').focus();
                            }
                            else{
                                // alert('لا يوجد هذا الصنف')
                            document.getElementById('barcodeID').value = "";
                            document.getElementById('barcodeID').focus();
                            }

                        }
                    });
                    }
                }
                function addItem(id) {        
                    const formData = {
                        'BillID': id
                    };
                    if(id){
                    $.ajax({
                        url: "./getBillRow.php",
                        type: "post",
                        data: formData,
                        success: function(d) {
                            const lst = JSON.parse(d)
                            if (lst.status === 200) {
                            $('#myTable').append('<tr><td>' + lst.id + '</td><td style="width: 486px;">' + lst.item_name + '</td><td class="price">' + lst.price + '</td><td><input type="text" class="identifier" value="1" /></td><td class="total">' + lst.price + '</td><td><a class="rmv btn btn-danger btn-xs delete"supp_id="2"><i class="fa   fa-times" aria-hidden="true"></i>  حذف</i></a></td></tr>');
                            // table.row.add([lst.id,lst.item_barcode,lst.item_name,'<input type="text" />',lst.price]).draw();
                            const sum11 = $('#sum1').html()
                            const vat1 = $('#vat').html()
                            const discount11 = $("#discount").val();
                            const total1 = $('#total').html()
                            const vatValue = $('#vatValue').html()
                            let suum = 0
                            let vattt = 0
                            let totall555 = 0
                            if (sum1) {
                                suum = parseFloat(sum11);
                                suum = suum + parseFloat(lst.price)
                            } else {
                                suum = suum + parseFloat(lst.price)
                            }

                            if(vatValue){
                            vattt = (parseFloat(suum) * vatValue) / 100;
                            totall555 = parseFloat(suum) + parseFloat(vattt);
                            }
                            let discount12 = 0;
                            if (discount11) {
                                discount12 = parseFloat(discount11);
                            }
                            const totall5 = totall555 - discount12;
                            const summ1 = Math.round(suum) 
                            const vatt1 = Math.round(vattt) 
                            const totall55 = Math.round(totall5) 
                            $('#sum1').html(summ1)
                            $('#vat').html(vatt1)
                            $('#total').html(totall55)
                            }
                            else{
                                alert('لا يوجد هذا الصنف')
                            }

                        }
                    });
                }
                };
                function addItemServ(id) {        
                    const formData = {
                        'BillID': id
                    };
                    if(id){
                    $.ajax({
                        url: "./getBillRowServ.php",
                        type: "post",
                        data: formData,
                        success: function(d) {
                            const lst = JSON.parse(d)
                            $('#myTable').append('<tr><td>' + lst.id + '</td><td style="width: 486px;">' + lst.service_name + '</td><td class="price">' + lst.service_price + '</td><td><input type="text" class="identifier" value="1" /></td><td class="total">' + lst.service_price + '</td><td><a class="rmv btn btn-danger btn-xs delete"supp_id="2"><i class="fa   fa-times" aria-hidden="true"></i>  حذف</i></a></td></tr>');
                            // table.row.add([lst.id,lst.item_barcode,lst.item_name,'<input type="text" />',lst.price]).draw();
                            const sum11 = $('#sum1').html()
                            const vat1 = $('#vat').html()
                            const discount11 = $("#discount").val();
                            const total1 = $('#total').html()
                            const vatValue = $('#vatValue').html()
                            let suum = 0
                            let vattt = 0
                            let totall555 = 0
                            if (sum1) {
                                suum = parseFloat(sum11);
                                suum = suum + parseFloat(lst.service_price)
                            } else {
                                suum = suum + parseFloat(lst.service_price)
                            }

                            if(vatValue){
                            vattt = (parseFloat(suum) * vatValue) / 100;
                             totall555 = parseFloat(suum) + parseFloat(vattt);
                            }
                            let discount12 = 0;
                            if (discount11) {
                                discount12 = parseFloat(discount11);
                            }
                            const totall5 = totall555 - discount12;
                            const summ1 = Math.round(suum) 
                            const vatt1 = Math.round(vattt) 
                            const totall55 = Math.round(totall5) 
                            $('#sum1').html(summ1)
                            $('#vat').html(vatt1)
                            $('#total').html(totall55)
                        }
                    });
                }
                };
            $(function() {
                document.getElementById('barcodeID').focus();
                $(document).on('click', '.rmv', function(event) {
                    alert('سوف يتم حذف الصنف')
                    event.target.closest('tr').remove();
                    let summ = 0;
                    $("#itemsT tbody tr").each(function() {
                        var textval = $(this).find("td").eq(3).find(":text").val();
                        var textPrice = $(this).find("td").eq(4).html();
                        summ = parseFloat(summ) + parseFloat(textPrice)
                    });
                    if (summ > 0) {
                        const vat1 = $('#vat').html()
                        const discount11 = $("#discount").val();
                        const total1 = $('#total').html()
                        let vatValue = $('#vatValue').html()
                        if(!vatValue){vatValue = 0}
                        const vatt = (parseFloat(summ) * vatValue) / 100;
                        const sss = parseFloat(summ) + parseFloat(vatt);
                        let discount12 = 0;
                        if (discount11) {
                            discount12 = parseFloat(discount11);
                        }
                        const totall5 = sss - discount12;
                        const summ1 = Math.round(summ) 
                        const vatt1 = Math.round(vatt) 
                        const totall55 = Math.round(totall5) 
                        $('#sum1').html(summ1,2)
                        $('#vat').html(vatt1,2)
                        $('#total').html(totall55,2)
                    }
                });
                $(document).on('keyup', '.identifier', function(event) {
                    let cunt = $(this).val();
                    if (cunt) {
                        const prH = $(this).closest('tr').find(".price").html();
                        //const pr = $(this).closest('tr').find(".price").html();
                        const total = parseFloat(cunt) * parseFloat(prH)
                        $(this).closest('tr').find(".total").html(total);
                    } else {
                        cunt = 1
                        const prH = $(this).closest('tr').find(".price").html();
                        //const pr = $(this).closest('tr').find(".price").html();
                        const total = cunt * parseFloat(prH)
                        $(this).closest('tr').find(".total").html(total);
                    }
                    // $('#itemsT > tbody  > tr').each(function(index, tr) { 
                    let summ = 0;
                    $("#itemsT tbody tr").each(function() {
                        var textval = $(this).find("td").eq(3).find(":text").val();
                        var textPrice = $(this).find("td").eq(4).html();
                        summ = parseFloat(summ) + parseFloat(textPrice)
                    });
                    if (summ > 0) {
                        const vat1 = $('#vat').html()
                        let vatValue = $('#vatValue').html()
                        const discount11 = $("#discount").val();
                        const total1 = $('#total').html()
                        if(!vatValue){vatValue = 0}
                        const vatt = (parseFloat(summ) * vatValue) / 100;
                        const sss = parseFloat(summ) + parseFloat(vatt);
                        let discount12 = 0;
                        if (discount11) {
                            discount12 = parseFloat(discount11);
                        }
                        const totall5 = sss - discount12;
                        const summ1 = Math.round(summ) 
                        const vatt1 = Math.round(vatt) 
                        const totall55 = Math.round(totall5) 
                        $('#sum1').html(summ1)
                        $('#vat').html(vatt1)
                        $('#total').html(totall55)
                    }

                });
                $("#discount").on("keyup", function(event) {
                    const sum11 = $('#sum1').html()
                    let vatValue = $('#vatValue').html()
                    if(!vatValue){vatValue = 0}
                    let suum = parseFloat(sum11);
                    if (suum > 0) {
                        const vat1 = $('#vat').html()
                        const discount11 = $("#discount").val();
                        const total1 = $('#total').html()
                        const vatt = (parseFloat(suum) * vatValue) / 100;
                        const sss = parseFloat(suum) + parseFloat(vatt);
                        let discount12 = 0;
                        if (discount11) {
                            discount12 = parseFloat(discount11);
                        }
                        const totall5 = sss - discount12;
                        const summ1 = Math.round(suum) 
                        const vatt1 = Math.round(vatt) 
                        const totall55 = Math.round(totall5) 
                        $('#sum1').html(summ1)
                        $('#vat').html(vatt1)
                        $('#total').html(totall55)
                    }
                });

                $("#add_bill").on("click", function(event) {

                    location.reload();
                });
                $("#save_bill").on("click", function(event) {
                    const cv = $("#billId").html();
                    if(cv == " "){

                    
                    event.preventDefault();
                    const customer_name = $("#customerName").val();
                    const Mobile = $("#Mobile").val();
                    const customer_address1 = $("#customer_address").val();
                    const customer_TaxNum1 = $("#customer_TaxNum").val();
                    const iban = $("#iban").val();
                    const sum1 = $("#sum1").html();
                    const vat = $("#vat").html();
                    const discount = $("#discount").val();
                    const total = $("#total").html();
                    const pay_method = $("input[name='pay_method']:checked").val();
                    const note = $("#note").val();
                    // const product_count = $( "#product_count" ).val();

                    let arrLst = []
                    $("#itemsT tbody tr").each(function() {
                        var itemId = $(this).find("td").eq(0).html();
                        var item_name = $(this).find("td").eq(1).html();
                        var textPrice = $(this).find("td").eq(2).html();
                        var textAmount = $(this).find("td").eq(3).find(":text").val();
                        var textTotal = $(this).find("td").eq(4).html();

                        const lst1 = {
                            'id': itemId,
                            'item': item_name,
                            'price': textPrice,
                            'amount': textAmount,
                            'total': textTotal
                        }
                        arrLst.push(lst1)
                    });
                    if (arrLst.length > 0) {
                        const formData = {
                            'customer_name': customer_name,
                            'Mobile': Mobile,
                            'customer_address': customer_address1,
                            'customer_TaxNum': customer_TaxNum1,
                            'iban': iban,
                            'sum1': sum1,
                            'vat': vat,
                            'discount': discount,
                            'note': note,
                            'total': total,
                            'pay_method': pay_method,
                            'items': arrLst
                        };
                        $.ajax({
                            url: "./save_bill.php",
                            type: "post",
                            data: formData,
                            success: function(d) {
                                console.log(d)
                                const lst = JSON.parse(d)
                                $("#billId").html(lst.BillID);
                                $('#samm_code').html(lst.samm_code);

                            }
                        });
                    } else {
                        alert('يجب اضافة اصناف قبل حفظ الفاتورة');
                    }
                }
                });

                $("#print_bill").on("click", function(event) {
                    const bid = $("#billId").html();
                    if (bid != " ") {
                        //location.replace("./printBill.php");
                        window.open("./printBill.php?id="+bid+"", '_blank');
                    } else {
                        event.preventDefault();
                        const customer_name = $("#customerName").val();
                        const Mobile = $("#Mobile").val();
                        const customer_address = $("#customer_address").val();
                        const customer_TaxNum = $("#customer_TaxNum").val();
                        const iban = $("#iban").val();
                        const sum1 = $("#sum1").html();
                        const vat = $("#vat").html();
                        const discount = $("#discount").val();
                        const total = $("#total").html();
                        const pay_method = $("input[name='pay_method']:checked").val();
                        const note = $("#note").val();
                        // const product_count = $( "#product_count" ).val();

                        let arrLst = []
                        $("#itemsT tbody tr").each(function() {
                            var itemId = $(this).find("td").eq(0).html();
                            var item_barcode = $(this).find("td").eq(1).html();
                            var item_name = $(this).find("td").eq(2).html();
                            var textval = $(this).find("td").eq(3).find(":text").val();
                            var textPrice = $(this).find("td").eq(4).html();
                            const lst1 = {
                                'id': itemId,
                                'barcode': item_barcode,
                                'item': item_name,
                                'ItemCount': textval,
                                'itemPrice': textPrice
                            }
                            arrLst.push(lst1)
                        });
                        if (arrLst.length > 0) {
                            const formData = {
                                'customer_name': customer_name,
                                'Mobile': Mobile,
                                'customer_address': customer_address,
                                'customer_TaxNum': customer_TaxNum,
                                'sum1': sum1,
                                'iban': iban,
                                'vat': vat,
                                'note': note,
                                'discount': discount,
                                'total': total,
                                'pay_method': pay_method,
                                'items': arrLst
                            };
                            $.ajax({
                                url: "./save_bill.php",
                                type: "post",
                                data: formData,
                                success: function(d) {
                                    
                                    const lst = JSON.parse(d)
                                    $("#billId").html(lst.BillID);
                                    window.open("./printBill.php?id="+lst.BillID+"", '_blank');
                                }
                            });
                           
                        } else {
                            alert('يجب اضافة اصناف قبل حفظ الفاتورة');
                        }
                       
                    }

                });
                $("#print_billBarcode").on("click", function(event) {
                    const bid = $("#billId").html();
                    if (bid != " ") {
                        //location.replace("./printBill.php");
                        window.open("./printBillHeat.php?id="+bid+"", '_blank');
                    } else {
                        event.preventDefault();
                        const customer_name = $("#customerName").val();
                        const Mobile = $("#Mobile").val();
                        const customer_address = $("#customer_address").val();
                        const customer_TaxNum = $("#customer_TaxNum").val();
                        const iban = $("#iban").val();
                        const sum1 = $("#sum1").html();
                        const vat = $("#vat").html();
                        const discount = $("#discount").val();
                        const total = $("#total").html();
                        const pay_method = $("input[name='pay_method']:checked").val();
                        const note = $("#note").val();
                        // const product_count = $( "#product_count" ).val();

                        let arrLst = []
                        $("#itemsT tbody tr").each(function() {
                            var itemId = $(this).find("td").eq(0).html();
                            var item_barcode = $(this).find("td").eq(1).html();
                            var item_name = $(this).find("td").eq(2).html();
                            var textval = $(this).find("td").eq(3).find(":text").val();
                            var textPrice = $(this).find("td").eq(4).html();
                            const lst1 = {
                                'id': itemId,
                                'barcode': item_barcode,
                                'item': item_name,
                                'ItemCount': textval,
                                'itemPrice': textPrice
                            }
                            arrLst.push(lst1)
                        });
                        if (arrLst.length > 0) {
                            const formData = {
                                'customer_name': customer_name,
                                'Mobile': Mobile,
                                'customer_address': customer_address,
                                'customer_TaxNum': customer_TaxNum,
                                'sum1': sum1,
                                'iban': iban,
                                'vat': vat,
                                'note': note,
                                'discount': discount,
                                'total': total,
                                'pay_method': pay_method,
                                'items': arrLst
                            };
                            $.ajax({
                                url: "./save_bill.php",
                                type: "post",
                                data: formData,
                                success: function(d) {
                                    
                                    const lst = JSON.parse(d)
                                    $("#billId").html(lst.BillID);
                                    window.open("./printBill.php?id="+lst.BillID+"", '_blank');
                                }
                            });
                           
                        } else {
                            alert('يجب اضافة اصناف قبل حفظ الفاتورة');
                        }
                       
                    }

                });
                $("#print_billLetter").on("click", function(event) {
                    const bid = $("#billId").html();
                    if (bid != " ") {
                        //location.replace("./printBill.php");
                        window.open("./printBillLetterhead.php?id="+bid+"", '_blank');
                    } else {
                        event.preventDefault();
                        const customer_name = $("#customerName").val();
                        const Mobile = $("#Mobile").val();
                        const customer_address = $("#customer_address").val();
                        const customer_TaxNum = $("#customer_TaxNum").val();
                        const iban = $("#iban").val();
                        const sum1 = $("#sum1").html();
                        const vat = $("#vat").html();
                        const discount = $("#discount").val();
                        const total = $("#total").html();
                        const pay_method = $("input[name='pay_method']:checked").val();
                        const note = $("#note").val();
                        // const product_count = $( "#product_count" ).val();

                        let arrLst = []
                        $("#itemsT tbody tr").each(function() {
                            var itemId = $(this).find("td").eq(0).html();
                            var item_barcode = $(this).find("td").eq(1).html();
                            var item_name = $(this).find("td").eq(2).html();
                            var textval = $(this).find("td").eq(3).find(":text").val();
                            var textPrice = $(this).find("td").eq(4).html();
                            const lst1 = {
                                'id': itemId,
                                'barcode': item_barcode,
                                'item': item_name,
                                'ItemCount': textval,
                                'itemPrice': textPrice
                            }
                            arrLst.push(lst1)
                        });
                        if (arrLst.length > 0) {
                            const formData = {
                                'customer_name': customer_name,
                                'Mobile': Mobile,
                                'customer_address': customer_address,
                                'customer_TaxNum': customer_TaxNum,
                                'sum1': sum1,
                                'iban': iban,
                                'vat': vat,
                                'note': note,
                                'discount': discount,
                                'total': total,
                                'pay_method': pay_method,
                                'items': arrLst
                            };
                            $.ajax({
                                url: "./save_bill.php",
                                type: "post",
                                data: formData,
                                success: function(d) {
                                    
                                    const lst = JSON.parse(d)
                                    $("#billId").html(lst.BillID);
                                    window.open("./printBill.php?id="+lst.BillID+"", '_blank');
                                }
                            });
                           
                        } else {
                            alert('يجب اضافة اصناف قبل حفظ الفاتورة');
                        }
                       
                    }

                });
                $("#print_billcustom").on("click", function(event) {
                    const bid = $("#billId").html();
                    if (bid != " ") {
                        //location.replace("./printBill.php");
                        window.open("./printBillCustom.php?id="+bid+"", '_blank');
                    } else {
                        event.preventDefault();
                        const customer_name = $("#customerName").val();
                        const Mobile = $("#Mobile").val();
                        const customer_address = $("#customer_address").val();
                        const customer_TaxNum = $("#customer_TaxNum").val();
                        const iban = $("#iban").val();
                        const sum1 = $("#sum1").html();
                        const vat = $("#vat").html();
                        const discount = $("#discount").val();
                        const total = $("#total").html();
                        const pay_method = $("input[name='pay_method']:checked").val();
                        const note = $("#note").val();
                        // const product_count = $( "#product_count" ).val();

                        let arrLst = []
                        $("#itemsT tbody tr").each(function() {
                            var itemId = $(this).find("td").eq(0).html();
                            var item_barcode = $(this).find("td").eq(1).html();
                            var item_name = $(this).find("td").eq(2).html();
                            var textval = $(this).find("td").eq(3).find(":text").val();
                            var textPrice = $(this).find("td").eq(4).html();
                            const lst1 = {
                                'id': itemId,
                                'barcode': item_barcode,
                                'item': item_name,
                                'ItemCount': textval,
                                'itemPrice': textPrice
                            }
                            arrLst.push(lst1)
                        });
                        if (arrLst.length > 0) {
                            const formData = {
                                'customer_name': customer_name,
                                'Mobile': Mobile,
                                'customer_address': customer_address,
                                'customer_TaxNum': customer_TaxNum,
                                'sum1': sum1,
                                'iban': iban,
                                'vat': vat,
                                'note': note,
                                'discount': discount,
                                'total': total,
                                'pay_method': pay_method,
                                'items': arrLst
                            };
                            $.ajax({
                                url: "./save_bill.php",
                                type: "post",
                                data: formData,
                                success: function(d) {
                                    
                                    const lst = JSON.parse(d)
                                    $("#billId").html(lst.BillID);
                                    window.open("./printBill.php?id="+lst.BillID+"", '_blank');
                                }
                            });
                           
                        } else {
                            alert('يجب اضافة اصناف قبل حفظ الفاتورة');
                        }
                       
                    }

                });
            })
        </script>
        </div>
</body>

</html>