<?php
session_start();
include("../conn.php");


if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$sql1="SELECT *FROM  papers WHERE paper_id =".$_GET['id'];
$result1 = mysqli_query($con,$sql1);
$row1=mysqli_fetch_array($result1);

if(isset($_POST['save']))
{
   
$paper_customer=$_POST['paper_customer'];
$paper_date=$_POST['paper_date'];
$paper_cat=$_POST['paper_cat'];
$paper_amount=$_POST['paper_amount'];
$pay_type=$_POST['pay_type'];
$shek_num=$_POST['shek_num'];
$almohasel=$_POST['almohasel'];
$almostalem=$_POST['almostalem'];
$bank=$_POST['bank'];
$payfor=$_POST['payfor'];
 $sql="UPDATE papers SET paper_customer='$paper_customer',paper_date='$paper_date',paper_amount='$paper_amount',paper_cat='$paper_cat',pay_type='$pay_type',shek_num='$shek_num',almohasel='$almohasel',almostalem='$almostalem',bank='$bank',payfor='$payfor' WHERE paper_id =".$_GET['id'];
 mysqli_query($con,$sql); 
header("location:show_paper1.php");
 
}

?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

  </head>
  <body>
  <section class="body" style="direction: rtl;">

<!-- start: header -->
<?php include("header.php"); ?>
<!-- end: header -->

<div class="inner-wrapper">
  <!-- start: sidebar -->
 <?php include("side.php"); ?>
  <!-- end: sidebar -->

  <section role="main" class="content-body">
   

    <!-- start: page -->
       <section class="panel">
       <header class="panel-heading">
          <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
            <a href="#" class="fa fa-times"></a>
          </div>
          <div>
             <a id="print_bill" class='btn btn-warning btn-xs ' href='add_paper1.php' style="float: left;"><i class="fa    fa-print" aria-hidden="true"></i> طباعة
           </a>
          </div>
      
          <h3 class="panel-title"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>  إضافة سند قبض</h3>
        </header>
      <div class="panel-body ">
   <form method="post" name="form-category-add" enctype="multipart/form-data" id="form-article-add">


  <div class="row">

  <div class="col-sm-12 col-xs-12">
      <div>
        <label>كود السند</label>
        <input readonly type="text" name="id" style="background-color: #1c1b1b;" value="<?php echo $row1["paper_id"]  ?>"  class="form-control"required=""  />
      </div>
      </div>  
      <div class="col-sm-6 col-xs-12">
      <div>
        <label>وذلك مقابل</label>
        <input type="text" name="payfor" value="<?php echo $row1["payfor"]  ?>" class="form-control"required=""  />
      </div>
      </div> 
      <div class="col-sm-6 col-xs-12">
      <div>
         <label>اسم العميل</label>
         <input type="text" name="paper_customer" value="<?php echo $row1["paper_customer"]  ?>" class="form-control" required=""/>
      </div>
      </div>   

      <div class="col-sm-6 col-xs-12">
      <div>
        <label>تصنيف السند</label>
        <input type="text" name="paper_cat" value="<?php echo $row1["paper_cat"]  ?>" class="form-control"required=""  />
      </div>
      </div>
      <div class="col-sm-6 col-xs-12">
      <div>
        <label>المبلغ</label>
        <input type="text" name="paper_amount" value="<?php echo $row1["paper_amount"]  ?>"   class="form-control"required=""  />
      </div>
      </div>
  
      <div class="col-sm-6 col-xs-12 ">       
      <div>
        <label>التاريخ</label>
        <input type="date" name="paper_date" value="<?php echo $row1["paper_date"]  ?>" class="form-control"required=""  />
      </div>
      </div>
      <div class="col-sm-6 col-xs-12 ">       
      <div>
        <label>المستلم</label>
        <input type="text" name="almostalem" value="<?php echo $row1["almostalem"]  ?>" class="form-control"required=""  />
      </div>
      </div>
      <div class="col-sm-6 col-xs-12 ">       
      <div>
         <label> المحصل</label>
         <input type="text" name="almohasel" value="<?php echo $row1["almohasel"]  ?>" class="form-control" required=""/>
      </div>
      </div>

      <div class="col-sm-6 col-xs-12 ">   
      <div>
        <label>رقم الشيك</label>
        <input type="text" name="shek_num" value="<?php echo $row1["shek_num"]  ?>" class="form-control" />
      </div>
      </div>
      <div class="col-sm-6 col-xs-12 ">   
      <div>
        <label> مسحوب على بنك</label>
        <input type="text" name="bank" value="<?php echo $row1["bank"]  ?>" class="form-control"  />
      </div>
      </div>
      <div class="col-sm-6 col-xs-12 ">   
      <label> طريقة الدفع</label>    
    <div class="form-check form-check-inline">
      <input class="form-check-input" type="radio" name="pay_type" id="inlineRadio1" value="نقداً" <?php echo ($row1["pay_type"]== 'نقداً' ? 'checked' : '');?>>
      <label class="form-check-label" for="inlineRadio1">نقداً</label>
    </div>
    <div class="form-check form-check-inline">    
      <input class="form-check-input" type="radio" name="pay_type" id="inlineRadio2" value="شيك" <?php echo ($row1["pay_type"]== 'شيك' ? 'checked' : '');?>>
      <label class="form-check-label" for="inlineRadio2">شيك</label>  
    </div>
    <div class="form-check form-check-inline">    
            <input class="form-check-input" type="radio" name="pay_type" id="inlineRadio3" value="تحويل" <?php echo ($row1["pay_type"]== 'تحويل' ? 'checked' : '');?>>
            <label class="form-check-label" for="inlineRadio3">تحويل</label>  
          </div>
      </div>
      </div>
         
          <div class="col-sm-12 col-xs-12">
          <hr>
          <div class="form-group text-center">
              <button type="submit" id="btn-svae-category" class="btn btn-warning" name="save">حفظ  بيانات   السند</button>
          </div>
      </div>
  </div>
</form>
        </div>
    </div>
    <!-- page end-->
  </div>
      </section>

            
           
      </div>

    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>


    <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../assets/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
            $(function() {
                $("#print_bill").on("click", function(event) {
                    const bid = "<?php echo $row1["paper_id"]; ?>";
                    if (bid != " ") {
                        //location.replace("./printBill.php");
                        window.open("./printPayReceipt.php?id="+bid+"", '_blank');
                    } else {
                       alert('لم يتم الحفظ')
                    }

                });
            })
        </script>

  </body>
</html>