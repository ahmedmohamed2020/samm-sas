<?php
include("../conn.php");


session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];
$sql="SELECT * from carts,customer where carts.company_id='$company_id' and  carts_type='service' and customer.customer_id=carts.carts_cus  ORDER BY `carts`.`carts_id` DESC ";
$result=mysqli_query($con,$sql);


?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

    <link href="../assets/advanced-datatable/css/demo_page.css" rel="stylesheet" />
  <link href="../assets/advanced-datatable/css/demo_table.css" rel="stylesheet" />
  <link rel="stylesheet" href="../assets/advanced-datatable/css/DT_bootstrap.css" />

  </head>
  <body>
    <section class="body">

      <!-- start: header -->
     <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
        

          <!-- start: page -->
            <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
                
                
                <h3 class="panel-title"><i class="fa fa-paperclip" aria-hidden="true"></i> فواتير الخدمات</h2>
              </header>
              <div class="panel-body table-responsive">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead style="    background: #34495e;">
                    <tr>
                      <tr>
                    
                    <th >#</th>
                    <th >العميل  </th>
                    <th >الهاتف  </th>
                    <th >قيمة الفاتورة  </th>
                    <th > التخفيض</th>
                    <th >القيمة النهائية  </th>
                    <th > السداد</th>
                    <th >الرقم الضريبي</th>
                    <th style="width: 200px;text-align: center;">.........</th>
                    

                </tr>
                    </tr>
                  </thead>
                  <tbody>
                       <?php    while ($row=mysqli_fetch_array($result)) {     ?>
            <tr>
                 <td><?php echo $row['carts_id']; ?></td>
                <td><?php echo $row['customer_name']; ?></td>
                 <td><?php echo $row['customer_phone']; ?></td>
               <td><?php echo $row['carts_price']; ?></td>
              
               
               <td><?php echo $row['carts_discount']; ?></td>
               <td><?php echo $row['carts_end_price']; ?></td>
               <td><?php echo $row['customer_tax']; ?></td>
               <td><?php echo $row['carts_date']; ?></td>
                <td>
                  
                   
                 <a href="delete_orders.php?id=<?php echo $row['carts_id']; ?>" class="btn btn-danger btn-xs delete" supp_id="2" >حزف</a>
                  <a href="order_ok1.php?id=<?php echo $row['carts_id']; ?>" class="btn btn-default btn-xs "  supp_id="2" >عرض   الاصناف</a>
                   <a href="chosePrint1.php?id=<?php echo $row['carts_id']; ?>" class="btn btn-primary btn-xs "
                            supp_id="2"
                           >طباعة</a>
                  </td>
            </tr>
            <?php }; ?>
                       
                  </tbody>
                </table>
              </div>
            </section>

            
           
      </div>

    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>
  </body>
</html>