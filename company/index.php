<?php
session_start();
include("../conn.php");


if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$sql1="SELECT count(paper_id) as paper from papers where company_id=".$_SESSION['company_id'];
$result1=mysqli_query($con,$sql1);
$row1=mysqli_fetch_array($result1);

$sql2="SELECT count(emp_id) as emps from emp where company_id=".$_SESSION['company_id'];
$result2=mysqli_query($con,$sql2);
$row2=mysqli_fetch_array($result2);

$sql3="SELECT count(customer_id) as customers from customer where company_id=".$_SESSION['company_id'];
$result3=mysqli_query($con,$sql3);
$row3=mysqli_fetch_array($result3);

$sql4="SELECT count(carts_id) as cart from carts where company_id=".$_SESSION['company_id'];
$result4=mysqli_query($con,$sql4);
$row4=mysqli_fetch_array($result4);
?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>
<style>
  .centrIndv{
    display: flex;
    justify-content: center;
    margin: 10px auto;
    float: right;
  }
  .circ{
    width: 136px;
    height: 104px;
    border-radius: 50%;
    background-color: #048135;
    padding-top: 22px;
    text-align: center;
    box-shadow: 2px 2px 24px 0px rgba(0,0,0,0.75) inset;
-webkit-box-shadow: 2px 2px 24px 0px rgba(0,0,0,0.75) inset;
-moz-box-shadow: 2px 2px 24px 0px rgba(0,0,0,0.75) inset;
}
.circ:hover{
font-weight: bold;
cursor: pointer;
}
.centr{
  text-align: center;
    font-size: 31px;
}
.normalA{
  text-decoration: none;
  color: #fff;
}
.normalA:hover{
  text-decoration: none;
  color: #fff;
}
</style>
  </head>
  <body>
    <section class="body" style="direction: rtl;">

      <!-- start: header -->
     <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
         

          <!-- start: page -->
             <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
            
                <h3 class="panel-title"><i class="fa fa-home" aria-hidden="true"></i> الصفحة الرئيسية</h3>
              </header>
            <div class="panel-body ">
              <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-6" style="margin: 20px auto;">
  <video width="320" height="240" autoplay muted style="width:100%;">
  <source src="../assets/videos/promo.mp4" type="video/mp4">
  <source src="../assets/videos/promo.ogg" type="video/ogg">
  Your browser does not support the video tag.
</video>
</div>
<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-6" style="text-align: center;margin-top: 23px;">
<img class="imgS" style="width: 419px;height: 234px;" src="../assets/images/<?php echo $_SESSION['Cpic']; ?>" >
</div>
              </div>
               <div class="row">   
               <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="Bills.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>إضافة فاتورة</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_Products.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>إضافة اصناف</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_service.php">
  <div class="circ ">
  <i class="fa fa-users centr"></i>
  <p>إضافة خدمات</p>
  </div>
</a>
</div>

<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_customer.php">
  <div class="circ ">
  <i class="fa fa-users centr"></i>
  <p>إضافة عميل</p>
  </div>
</a>
</div>

<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_sale.php">
  <div class="circ ">
  <i class="fa fa-users centr"></i>
  <p>إضافة مورد</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_user.php">
  <div class="circ ">
  <i class="fa fa-users centr"></i>
  <p>إضافة مستخدمين</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="company_Bills.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p> تقرير المبيعات</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_paper2.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>سند صرف</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="add_paper1.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>سند قبض</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="company_receipt.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p> تقرير سندات القبض</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="company_withdrow.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>تقرير  سندات الصرف</p>
  </div>
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 col-xs-3 centrIndv">
<a class="normalA" href="report_item.php">
  <div class="circ ">
  <i class="fa fa-money centr"></i>
  <p>تقرير المشتريات</p>
  </div>
</a>
</div>

               </div>
              <!-- <div class="col-md-12 col-lg-12 col-xl-12">
                <h5 class="text-semibold text-dark text-uppercase mb-md mt-lg" style="direction: rtl;"></h5>
                <div class="row">
                  <div class="col-md-6 col-xl-6">
                    <section class="panel">
                      <div class="panel-body bg-danger">
                        <div class="widget-summary">
                          <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon">
                              <i class="fa fa-shopping-cart"></i>
                            </div>
                          </div>
                          <div class="widget-summary-col">
                            <div class="summary">
                              <h4 class="title">عدد الفواتير</h4>
                              <div class="info">
                                <strong class="amount"><?php echo $row4['cart']; ?></strong>
                              </div>
                            </div>
                            <div class="summary-footer">
                              <a class="text-uppercase"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="col-md-6 col-xl-6">
                    <section class="panel">
                      <div class="panel-body bg-secondary">
                        <div class="widget-summary">
                          <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon">
                              <i class="fa fa-money"></i>
                            </div>
                          </div>
                          <div class="widget-summary-col">
                            <div class="summary">
                              <h4 class="title">عدد السندات</h4>
                              <div class="info">
                                <strong class="amount"><?php echo $row1['paper']; ?></strong>
                              </div>
                            </div>
                            <div class="summary-footer">
                              <a class="text-uppercase"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="col-md-6 col-xl-6">
                    <section class="panel">
                      <div class="panel-body bg-tertiary">
                        <div class="widget-summary">
                          <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon">
                              <i class="fa fa-users"></i>
                            </div>
                          </div>
                          <div class="widget-summary-col">
                            <div class="summary">
                              <h4 class="title">عدد الموظفين</h4>
                              <div class="info">
                                <strong class="amount"><?php echo $row2['emps']; ?></strong>
                              </div>
                            </div>
                            <div class="summary-footer">
                              <a class="text-uppercase"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                  <div class="col-md-6 col-xl-6">
                    <section class="panel">
                      <div class="panel-body bg-quartenary">
                        <div class="widget-summary">
                          <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon">
                              <i class="fa  fa-briefcase"></i>
                            </div>
                          </div>
                          <div class="widget-summary-col">
                            <div class="summary">
                              <h4 class="title">عدد العملاء</h4>
                              <div class="info">
                                <strong class="amount"><?php echo $row3['customers']; ?></strong>
                              </div>
                            </div>
                            <div class="summary-footer">
                              <a class="text-uppercase"></a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div> -->
              </div>
          </div>
          <!-- page end-->
        </div>
            </section>

            
           
      </div>
    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>


    <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../assets/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
  <script type="text/javascript">
    /* Formating function for row details */
    function fnFormatDetails(oTable, nTr) {
      var aData = oTable.fnGetData(nTr);
      var sOut = '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;font-size: revert;    font-weight: bold;">';
      sOut += '<tr><td>اسم الشركة :</td><td>' + aData[1] + ' </td></tr>';
      sOut += '<tr><td>العنوان :</td><td>' + aData[2] + ' </td></tr>';
      sOut += '<tr><td>الهاتف :</td><td>' + aData[3] + ' </td></tr>';
      sOut += '<tr><td>الايميل  :</td><td>' + aData[4] + ' </td></tr>';
      sOut += '<tr><td>كلمة المرور  :</td><td>' + aData[5] + ' </td></tr>';
      sOut += '<tr><td>باقة العملاء  :</td><td>' + aData[6] + ' </td></tr>';
      sOut += '<tr><td>الحالة  :</td><td>' + aData[7] + ' </td></tr>';
      sOut += '</table>';

      return sOut;
    }

    $(document).ready(function() {
      /*
       * Insert a 'details' column to the table
       */
      var nCloneTh = document.createElement('th');
      var nCloneTd = document.createElement('td');
      nCloneTd.innerHTML = '<img src="../assets/advanced-datatable/images/details_open.png">';
      nCloneTd.className = "center";

      $('#hidden-table-info thead tr').each(function() {
        this.insertBefore(nCloneTh, this.childNodes[0]);
      });

      $('#hidden-table-info tbody tr').each(function() {
        this.insertBefore(nCloneTd.cloneNode(true), this.childNodes[0]);
      });

      /*
       * Initialse DataTables, with no sorting on the 'details' column
       */
      var oTable = $('#hidden-table-info').dataTable({
        "aoColumnDefs": [{
          "bSortable": false,
          "aTargets": [0]
        }],
        "aaSorting": [
          [1, 'asc']
        ]
      });

      /* Add event listener for opening and closing details
       * Note that the indicator for showing which row is open is not controlled by DataTables,
       * rather it is done here
       */
      $('#hidden-table-info tbody td img').live('click', function() {
        var nTr = $(this).parents('tr')[0];
        if (oTable.fnIsOpen(nTr)) {
          /* This row is already open - close it */
          this.src = "../assets/advanced-datatable/images/details_open.png";
          oTable.fnClose(nTr);
        } else {
          /* Open this row */
          this.src = "../assets/advanced-datatable/images/details_close.png";
          oTable.fnOpen(nTr, fnFormatDetails(oTable, nTr), 'details');
        }
      });
    });


        $(function () {
            setInterval(loadDateTime, 1000);
            function loadDateTime() {
                const event = new Date();
                const options = {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                }
                $('#dte').html(event.toLocaleDateString('ar-EG', options));
                $('#dt').html(event.toLocaleDateString('ar-SA', options));
                $('#tm').html(event.toLocaleTimeString('ar-SA'));

            }
        })

  </script>
  </body>
</html>