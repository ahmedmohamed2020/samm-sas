<?php
include("../conn.php");
include("../barcode.php");
include("../num_to_ar.php");
$generator = new barcode_generator();
$options = array();
// $generator->output_image('svg','ean-13-pad', '6225000351646', $options);
session_start();
if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}
$company_id=$_SESSION['company_id'];

// $sql1="SELECT *FROM  info ";
// $result1 = mysqli_query($con,$sql1);
// $row1=mysqli_fetch_array($result1);

// $sql2="SELECT *FROM  carts,emp,customer where customer.customer_id=carts.carts_cus and emp_id=carts_emp_id and carts_id= ".$_GET['id'];
// $result2 = mysqli_query($con,$sql2);
// $row2=mysqli_fetch_array($result2);


$sql4="SELECT * FROM  producats where company_id=".$company_id;
$result4 = mysqli_query($con,$sql4);


$i=1;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>فاتورة مبيعات</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="../css2/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../css2/adminlte.min.css">
  <link rel="stylesheet" href="../css2/printbarcode.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
    .rowss{
      direction: rtl;
      text-align: right;
      margin: 16px;
    }
    .algn{
      text-align: right;
    }
    .algnC{
      text-align: center;
    }
    .centerdv{
      display: flex;
    justify-content: center;
    }
    .titl{
      text-decoration: underline;
    font-size: 22px;
    }
    .backG{
      background-color: #d0d0d0;
    }
    .mr{
      margin-top: 32px;
    }
    .imgS{
      width:180px;
      height: 86px;
    }
    .imgSQR{
      width:235px;
      height: 235px;
    }
    .txtCenter{
      font-size: 18px;
      font-weight: bold;
    }
    table{
      width: 100%;
    }
    svg{
      width: 260px !important;
      height: 103px !important;
      margin-left: -26px;
    }
  </style>

</head>
<body>
<?php    while ($row=mysqli_fetch_array($result4)) {     ?>
  <div class="page2">
  <div class="txtCenter" style="text-align: center;"><?php echo $row['item_name']; ?></div> 
  <div class="txtCenter" style="text-align: center;direction:rtl;"> <span> <?php echo $row['price']; ?> </span> <span> ريال </span> </div> 
  <p><img src="barcode.php?f=png&s=code-128&d=<?php echo $row['item_barcode']; ?>&h=50px&w=250px&pl=-20"></p>
</div>
<?php    }  ?>
<!-- ./wrapper -->

<script type="text/javascript">
  window.addEventListener("load", window.print());
</script>
</body>
</html>

