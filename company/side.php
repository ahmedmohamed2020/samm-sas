 <aside id="sidebar-left" class="sidebar-left">
        
          <div class="sidebar-header">
            <div class="sidebar-title">
              
            </div>
            <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
              <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
            </div>
          </div>
        
          <div class="nano">
            <div class="nano-content">
              <nav id="menu" class="nav-main" role="navigation">
                <ul class="nav nav-main">
                  <li>
                    <a href="index.php">
                      <i class="fa fa-home" aria-hidden="true"></i>
                      <span>الصفحة الرئيسية</span>
                    </a>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-copy" aria-hidden="true"></i>
                      <span>ادارة المبيعات</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="add_dept.php">
                          <i class="fa fa-paste" aria-hidden="true"></i>
                           <span>الاقسام</span>  
                        </a>
                      </li>
                      <li>
                        <a href="show_sale.php">
                          <i class="fa fa-file-text" aria-hidden="true"></i>
                           <span>الموردين</span>  
                        </a>
                      </li>
                      <li>
                        <a href="show_item.php">
                          <i class="fa fa-truck" aria-hidden="true"></i>
                           <span>المشتريات</span>  
                        </a>
                      </li>
                      <li>
                        <a href="Bills.php">
                          <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                           <span>المبيعات</span>  
                        </a>
                      </li>
                      <li>
                        <a href="show_orders.php">
                          <i class="fa fa-paperclip" aria-hidden="true"></i>
                           <span>ادارة فواتير المبيعات</span>  
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent  nav-active">
                    <a>
                      <i class="fa fa-table" aria-hidden="true"></i>
                      <span> الخدمات</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="show_service.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الخدمات
                        </a>
                      </li>
                       <!-- <li>
                        <a href="show_orders1.php">
                          <i class="fa fa-paperclip" aria-hidden="true"></i>
                           <span>ادارة فواتير الخدمات</span>  
                        </a>
                      </li>                       -->
                    </ul>
                  </li> 
                  <li class="nav-parent  nav-active">
                    <a>
                      <i class="fa fa-table" aria-hidden="true"></i>
                      <span>الاصناف</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="show_Products.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الاصناف
                        </a>
                      </li>
                      <li>
                        <a href="add_Cat.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الفئات
                        </a>
                      </li>
                      <li>
                        <a href="add_Type.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الانواع
                        </a>
                      </li>
                      <li>
                        <a href="add_MainUom.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الوحدات الرئيسية
                        </a>
                      </li> 
                      <li>
                        <a href="add_SubUom.php">
                          <i class="fa fa-briefcase" aria-hidden="true"></i>
                           الوحدات الفرعية
                        </a>
                      </li> 
                    </ul>
                  </li>
                  <li>
                    <a href="BillsScreen.php">
                      <i class="fa fa-briefcase" aria-hidden="true"></i>
                      <span> شاشة كاشير</span>
                    </a>
                  </li> 
                     <li>
                    <a href="show_customer.php">
                      <i class="fa fa-briefcase" aria-hidden="true"></i>
                      <span>ادارة العملاء</span>
                    </a>
                  </li>
                     <li>
                    <a href="show_paper_all.php">
                      <i class="fa fa-credit-card" aria-hidden="true"></i>
                      <span>ادارة جميع السندات</span>
                    </a>
                  </li>
                     <li>
                    <a href="show_paper1.php">
                      <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                      <span>ادارة سندات القبض</span>
                    </a>
                  </li>
                     <li>
                    <a href="show_paper2.php">
                      <i class="fa fa-suitcase" aria-hidden="true"></i>
                      <span>ادارة سندات الصرف</span>
                    </a>
                  </li>
                  <li>
                    <a href="show_users.php">
                      <i class="fa fa-users" aria-hidden="true"></i>
                      <span>المستخدمين</span>
                    </a>
                  </li>
                    <li>
                    <a href="sittings.php">
                      <i class="fa  fa-gears" aria-hidden="true"></i>
                      <span>الاعدادات</span>
                    </a>
                  </li>
                  <li>
                    <a href="show_sig.php">
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                      <span>الختم و التوقيع</span>
                    </a>
                  </li>
                  
                  <li class="nav-parent  nav-active">
                    <a>
                      <i class="fa fa-table" aria-hidden="true"></i>
                      <span>التقارير</span>
                    </a>
                    <ul class="nav nav-children">
                    <li>
                        <a href="company_Bills.php" target="_blanck">
                           تقرير المبيعات عن فترة
                        </a>
                      </li>
                      <li>
                        <a href="company_receipt.php" target="_blanck">
                           تقرير سندات القبض عن فترة
                        </a>
                      </li>
                      <li>
                        <a href="company_withdrow.php" target="_blanck">
                        تقرير سندات الصرف عن فترة
                        </a>
                      </li>
                      <li>
                        <a href="report_item.php" target="_blanck">
                           تقرير  المشتريات
                        </a>
                      </li>
                    </ul>
                  </li>
                  <!-- start: page 
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                      <span>Maps</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="maps-google-maps.html">
                           Basic
                        </a>
                      </li>
                      <li>
                        <a href="maps-google-maps-builder.html">
                           Map Builder
                        </a>
                      </li>
                      <li>
                        <a href="maps-vector.html">
                           Vector
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-columns" aria-hidden="true"></i>
                      <span>Layouts</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a href="layouts-default.html">
                           Default
                        </a>
                      </li>
                      <li>
                        <a href="layouts-boxed.html">
                           Boxed
                        </a>
                      </li>
                      <li>
                        <a href="layouts-menu-collapsed.html">
                           Menu Collapsed
                        </a>
                      </li>
                      <li>
                        <a href="layouts-scroll.html">
                           Scroll
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="nav-parent">
                    <a>
                      <i class="fa fa-align-left" aria-hidden="true"></i>
                      <span>Menu Levels</span>
                    </a>
                    <ul class="nav nav-children">
                      <li>
                        <a>First Level</a>
                      </li>
                      <li class="nav-parent">
                        <a>Second Level</a>
                        <ul class="nav nav-children">
                          <li class="nav-parent">
                            <a>Third Level</a>
                            <ul class="nav nav-children">
                              <li>
                                <a>Third Level Link #1</a>
                              </li>
                              <li>
                                <a>Third Level Link #2</a>
                              </li>
                            </ul>
                          </li>
                          <li>
                            <a>Second Level Link #1</a>
                          </li>
                          <li>
                            <a>Second Level Link #2</a>
                          </li>
                        </ul>
                      </li>
                      -->
                    </ul>
                  </li>
                  
                </ul>
              </nav>
        
              
        
              <div class="sidebar-widget widget-stats">
                <div class="widget-header">
                 
                  <div class="widget-toggle">+</div>
                </div>
                <div class="widget-content">
                  <ul>
                    
                  </ul>
                </div>
              </div>
            </div>
        
          </div>
        
        </aside>


<!-- Vendor -->
<script src="../assets/vendor/jquery/jquery.js"></script>
<script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
<script src="lib/jquery/jquery.min.js"></script>
<script type="text/javascript">
$(function () {
            setInterval(loadDateTime, 1000);
            function loadDateTime() {
                const event = new Date();
                const options = {
                    weekday: "long",
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                }
                $('#dte').html(event.toLocaleDateString('ar-EG', options));
                $('#dt').html(event.toLocaleDateString('ar-SA', options));
                $('#tm').html(event.toLocaleTimeString('ar-SA'));

            }
        })
</script>