<?php
session_start();
include("../conn.php");


if (!isset($_SESSION['admin'])) {
    header("location:../login.php");
}


$company_id=$_SESSION['company_id'];

if(isset($_POST['save']))
{



    
$item_name=$_POST['item_name'];
$item_bar=$_POST['item_bar'];
$item_quan=$_POST['item_quan'];
$item_unit=$_POST['item_unit'];
$item_price1=$_POST['item_price1'];
$x=($_POST['item_price1']/100)*$_POST['item_tax'];
$item_price2=$_POST['item_price1']+$x;



 $sql="INSERT INTO items(item_name,item_bar,item_quan,item_unit,item_price1,item_price2,item_type,company_id)VALUES('$item_name','$item_bar','$item_quan','$item_unit','$item_price1','$item_price2',1,'$company_id')";
 mysqli_query($con,$sql); 
header("location:show_item.php");
 
}

?>
<html class="fixed">
  <head>

    <!-- Basic -->
    <meta charset="UTF-8">

    <title>ساس للخدمات المحاسبية</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="okler.net">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" href="../assets/vendor/font-awesome/css/font-awesome.css" />
    <link rel="stylesheet" href="../assets/vendor/magnific-popup/magnific-popup.css" />
    <link rel="stylesheet" href="../assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

    <!-- Specific Page Vendor CSS -->
    <link rel="stylesheet" href="../assets/vendor/select2/select2.css" />
    <link rel="stylesheet" href="../assets/vendor/jquery-datatables-bs3/../assets/css/datatables.css" />

    <!-- Theme CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme.css" />

    <!-- Skin CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/skins/default.css" />

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="../assets/stylesheets/theme-custom.css">

    <!-- Head Libs -->
    <script src="../assets/vendor/modernizr/modernizr.js"></script>

  </head>
  <body>
    <section class="body" style="direction: rtl;">

      <!-- start: header -->
      <?php include("header.php"); ?>
      <!-- end: header -->

      <div class="inner-wrapper">
        <!-- start: sidebar -->
       <?php include("side.php"); ?>
        <!-- end: sidebar -->

        <section role="main" class="content-body">
         

          <!-- start: page -->
             <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">
                  <a href="#" class="fa fa-caret-down"></a>
                  <a href="#" class="fa fa-times"></a>
                </div>
            
                <h3 class="panel-title"><i class="fa fa-plus" aria-hidden="true"></i> اضافة مشتريات جديدة</h3>
              </header>
            <div class="panel-body ">
         <form method="post" name="form-category-add" enctype="multipart/form-data" id="form-article-add">


        <div class="row">

            
            <div class="col-sm-6 col-xs-12" style="float: right;">
            <div>
            <label>اسم الصنف</label>
            <input type="text" name="item_name"  class="form-control"  required="" />
            </div>
            <div>
            <label>الباركود </label>
            <input type="text" name="item_bar"  class="form-control"     required="" />
            </div>
            <div>
            <label>الكمية</label>
            <input type="number" name="item_quan"  class="form-control"     required="" />
            </div>
            </div>
            <div class="col-sm-6 col-xs-12 ">
            <div>
            <label>الوحدة</label>
            <input type="text" name="item_unit"  class="form-control" required="" />
            </div>
            <div>
            <label>سعر شراء الوحدة  بدون ضريبة</label>
            <input type="text" name="item_price1"  class="form-control" required="" />
            </div>
           <div>
            <label>الضريبة</label>
            <input type="number" name="item_tax"  class="form-control" value="15" readonly="" style="background-color: #35373a" />
            </div>
            </div>
          </div>

               
                <div class="col-sm-12 col-xs-12">
                <hr>

                <div class="form-group text-center">
                    <button type="submit" id="btn-svae-category" class="btn btn-warning" name="save">حفظ  المشتريات</button>
                </div>
            </div>
        </div>
    </form>
              </div>
          </div>
          <!-- page end-->
        </div>
            </section>

            
           
      </div>

    </section>

    <!-- Vendor -->
    <script src="../assets/vendor/jquery/jquery.js"></script>
    <script src="../assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
    <script src="../assets/vendor/bootstrap/js/bootstrap.js"></script>
    <script src="../assets/vendor/nanoscroller/nanoscroller.js"></script>
    <script src="../assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="../assets/vendor/magnific-popup/magnific-popup.js"></script>
    <script src="../assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Specific Page Vendor -->
    <script src="../assets/vendor/select2/select2.js"></script>
    <script src="../assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
    <script src="../assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js"></script>
    <script src="../assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>
    
    <!-- Theme Base, Components and Settings -->
    <script src="../assets/javascripts/theme.js"></script>
    
    <!-- Theme Custom -->
    <script src="../assets/javascripts/theme.custom.js"></script>
    
    <!-- Theme Initialization Files -->
    <script src="../assets/javascripts/theme.init.js"></script>


    <!-- Examples -->
    <script src="../assets/javascripts/tables/examples.datatables.default.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.row.with.details.js"></script>
    <script src="../assets/javascripts/tables/examples.datatables.tabletools.js"></script>


    <!-- js placed at the end of the document so the pages load faster -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="lib/jquery.dcjqaccordion.2.7.js"></script>
  <script src="lib/jquery.scrollTo.min.js"></script>
  <script src="lib/jquery.nicescroll.js" type="text/javascript"></script>
  <script type="text/javascript" language="javascript" src="../assets/advanced-datatable/js/jquery.dataTables.js"></script>
  <script type="text/javascript" src="../assets/advanced-datatable/js/DT_bootstrap.js"></script>
  <!--common script for all pages-->
  <script src="lib/common-scripts.js"></script>
  <!--script for this page-->
 
  </body>
</html>